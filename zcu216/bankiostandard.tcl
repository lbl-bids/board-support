proc bankiostandardproc { } {
dict set iostandard1v2_69 se LVCMOS12
dict set iostandard1v2_69 diff DIFF_SSTL12
dict set bankiostandard 69 $iostandard1v2_69
dict set iostandard1v2_68 se POD12
dict set iostandard1v2_68 diff DIFF_POD12
dict set bankiostandard 68 $iostandard1v2_68


dict set iostandard1v8_67 se LVCMOS18
dict set iostandard1v8_67 diff LVDS
dict set bankiostandard 67 $iostandard1v8_67
dict set iostandard1v8_66 se LVCMOS18
dict set iostandard1v8_66 diff LVDS
dict set bankiostandard 66 $iostandard1v8_66

dict set iostandard1v2_65 se LVCMOS12
dict set iostandard1v2_65 diff DIFF_SSTL12
dict set bankiostandard 65 $iostandard1v2_65
dict set iostandard1v2_64 se LVCMOS12
dict set iostandard1v2_64 diff DIFF_SSTL12
dict set bankiostandard 64 $iostandard1v2_64

dict set iostandard1v8 se LVCMOS18
dict set iostandard1v8 diff SUB_LVDS
dict set bankiostandard 84 $iostandard1v8
dict set bankiostandard 87 $iostandard1v8
dict set bankiostandard 88 $iostandard1v8
dict set bankiostandard 89 $iostandard1v8

dict set iostandardnone se none
dict set iostandardnone diff none
dict set bankiostandard 128 $iostandardnone
dict set bankiostandard 129 $iostandardnone
dict set bankiostandard 130 $iostandardnone
dict set bankiostandard 131 $iostandardnone
dict set bankiostandard 224 $iostandardnone
dict set bankiostandard 225 $iostandardnone
dict set bankiostandard 226 $iostandardnone
dict set bankiostandard 227 $iostandardnone
dict set bankiostandard 228 $iostandardnone
dict set bankiostandard 229 $iostandardnone
dict set bankiostandard 230 $iostandardnone
dict set bankiostandard 231 $iostandardnone
dict set bankiostandard 232 $iostandardnone
return $bankiostandard
}
