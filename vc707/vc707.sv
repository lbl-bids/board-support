module vc707 #(parameter USE_SMA_MGT="TRUE",parameter USE_SGMII_MGT="TRUE",parameter USE_SFP_MGT="TRUE",parameter USE_PCIE_MGT="TRUE",parameter USE_FMC1_MGT="TRUE",parameter USE_FMC2_MGT="TRUE")
(xc7vx485tffg1761pkg fpga,ivc707.hw hw);
vc707_v #(.USE_SMA_MGT(USE_SMA_MGT),.USE_SGMII_MGT(USE_SGMII_MGT),.USE_SFP_MGT(USE_SFP_MGT),.USE_PCIE_MGT(USE_PCIE_MGT),.USE_FMC1_MGT(USE_FMC1_MGT),.USE_FMC2_MGT(USE_FMC2_MGT))
vc707_v(
`include "fpga_vinst.vh"
,.hw(hw)
);
endmodule
module vc707_v #(parameter USE_SMA_MGT="TRUE",parameter USE_SGMII_MGT="TRUE",parameter USE_SFP_MGT="TRUE",parameter USE_PCIE_MGT="TRUE",parameter USE_FMC1_MGT="TRUE",parameter USE_FMC2_MGT="TRUE")
(//xc7vx485tffg1761pkg fpga
`include "fpga_vport.vh"
,ivc707.hw hw
);
xc7vx485tffg1761pkg fpga();
localparam USEMGTXRX113=(USE_SMA_MGT=="TRUE") |(USE_SGMII_MGT=="TRUE")|(USE_SFP_MGT=="TRUE") ? "TRUE": "FALSE";
localparam USEMGTXRX114=(USE_PCIE_MGT=="TRUE") ? "TRUE": "FALSE";
localparam USEMGTXRX115=(USE_PCIE_MGT=="TRUE") ? "TRUE": "FALSE";
localparam USEMGTXRX116=(USE_FMC2_MGT=="TRUE") ? "TRUE": "FALSE";
localparam USEMGTXRX117=(USE_FMC2_MGT=="TRUE") ? "TRUE": "FALSE";
localparam USEMGTXRX118=(USE_FMC1_MGT=="TRUE") ? "TRUE": "FALSE";
localparam USEMGTXRX119=(USE_FMC1_MGT=="TRUE") ? "TRUE": "FALSE";
`include "fpga_via.vh"
vc707_i #(.USE_SMA_MGT(USE_SMA_MGT),.USE_SGMII_MGT(USE_SGMII_MGT),.USE_SFP_MGT(USE_SFP_MGT),.USE_PCIE_MGT(USE_PCIE_MGT),.USE_FMC1_MGT(USE_FMC1_MGT),.USE_FMC2_MGT(USE_FMC2_MGT))
vc707_i(.fpga(fpga),.hw(hw));
endmodule

module vc707_i #(parameter USE_SMA_MGT="TRUE",parameter USE_SGMII_MGT="TRUE",parameter USE_SFP_MGT="TRUE",parameter USE_PCIE_MGT="TRUE",parameter USE_FMC1_MGT="TRUE",parameter USE_FMC2_MGT="TRUE")
(xc7vx485tffg1761pkg fpga,ivc707.hw hw);
// clock
//IBUFGDS #(.DIFF_TERM("TRUE")) ibuf_clk(.I(fpga.E19), .IB(fpga.E18), .O(hw.sysclk));
IBUFGDS ibuf_clk(.I(fpga.E19), .IB(fpga.E18), .O(hw.sysclk));

//assign  {hw.si5324_int_alm, hw.si5324_out_c_p, hw.si5324_out_c_n}={fpga.AU34, fpga.AD8, fpga.AD7};
viabus #(.WIDTH(3))
viasi5324 ({hw.si5324_int_alm},{fpga.AU34});
//assign {fpga.AW32, fpga.AW33}={hw.rec_clock_c_p, hw.rec_clock_c_n};
OBUFDS obufds_rec_clk(.I(hw.rec_clock),.O(fpga.AW32),.OB(fpga.AW33));
//assign fpga.AT36=hw.si5324_rst;
obufbus #(.WIDTH(1))
obufsi5324(.O(fpga.AT36),.I(hw.si5324_rst));

IBUFDS_GTE2 mgtrefclk_113_sgmii(.I(fpga.AH8),.IB(fpga.AH7),.O(hw.sgmiiclk),.ODIV2(),.CEB(1'b0));
IBUFDS_GTE2 mgtrefclk_113_sma(.I(fpga.AK8),.IB(fpga.AK7),.O(hw.sma_mgt_refclk),.ODIV2(),.CEB(1'b0));
IBUFDS_GTE2 mgtrefclk_114_si5324(.I(fpga.AD8),.IB(fpga.AD7),.O(hw.si5324_out_c),.ODIV2(),.CEB(1'b0));

//assign {hw.sma_mgt_refclk_n, hw.sma_mgt_refclk_p, hw.sma_mgt_rx_p, hw.sma_mgt_rx_n, hw.fmc1_hpc_gbtclk1_m2c_c_p, hw.fmc1_hpc_gbtclk1_m2c_c_n, hw.fmc1_hpc_gbtclk0_m2c_c_p, hw.fmc1_hpc_gbtclk0_m2c_c_n, hw.fmc2_hpc_gbtclk1_m2c_c_p, hw.fmc2_hpc_gbtclk1_m2c_c_n, hw.fmc2_hpc_gbtclk0_m2c_c_p, hw.fmc2_hpc_gbtclk0_m2c_c_n}={fpga.AK7, fpga.AK8, fpga.AN6, fpga.AN5, fpga.E10, fpga.E9, fpga.A10, fpga.A9, fpga.T8, fpga.T7, fpga.K8, fpga.K7};
viabus #(.WIDTH(4))
viamgt({
 hw.sma_mgt_rx_p
,hw.sma_mgt_rx_n
,hw.sma_mgt_tx_p
,hw.sma_mgt_tx_n
}
,{
 fpga.AN6
,fpga.AN5
,fpga.AP4
,fpga.AP3
});


// ddr3
viapairs #(.WIDTH(122)) ddr3_via({fpga.C29,hw.ddr3.reset_b,fpga.G19, hw.ddr3.clk1_p,fpga.F19, hw.ddr3.clk1_n,fpga.H19, hw.ddr3.clk0_p,fpga.G18, hw.ddr3.clk0_n,fpga.K19, hw.ddr3.cke0,fpga.J18, hw.ddr3.cke1,fpga.F20, hw.ddr3.we_b,fpga.E20, hw.ddr3.ras_b,fpga.K17, hw.ddr3.cas_b,fpga.H20, hw.ddr3.odt0,fpga.H18, hw.ddr3.odt1,fpga.G17, hw.ddr3.temp_event,fpga.N14, hw.ddr3.d0,fpga.N13, hw.ddr3.d1,fpga.L14, hw.ddr3.d2,fpga.M14, hw.ddr3.d3,fpga.M12, hw.ddr3.d4,fpga.N15, hw.ddr3.d5,fpga.M11, hw.ddr3.d6,fpga.L12, hw.ddr3.d7,fpga.K14, hw.ddr3.d8,fpga.K13, hw.ddr3.d9,fpga.H13, hw.ddr3.d10,fpga.J13, hw.ddr3.d11,fpga.L16, hw.ddr3.d12,fpga.L15, hw.ddr3.d13,fpga.H14, hw.ddr3.d14,fpga.J15, hw.ddr3.d15,fpga.E15, hw.ddr3.d16,fpga.E13, hw.ddr3.d17,fpga.F15, hw.ddr3.d18,fpga.E14, hw.ddr3.d19,fpga.G13, hw.ddr3.d20,fpga.G12, hw.ddr3.d21,fpga.F14, hw.ddr3.d22,fpga.G14, hw.ddr3.d23,fpga.B14, hw.ddr3.d24,fpga.C13, hw.ddr3.d25,fpga.B16, hw.ddr3.d26,fpga.D15, hw.ddr3.d27,fpga.D13, hw.ddr3.d28,fpga.E12, hw.ddr3.d29,fpga.C16, hw.ddr3.d30,fpga.D16, hw.ddr3.d31,fpga.A24, hw.ddr3.d32,fpga.B23, hw.ddr3.d33,fpga.B27, hw.ddr3.d34,fpga.B26, hw.ddr3.d35,fpga.A22, hw.ddr3.d36,fpga.B22, hw.ddr3.d37,fpga.A25, hw.ddr3.d38,fpga.C24, hw.ddr3.d39,fpga.E24, hw.ddr3.d40,fpga.D23, hw.ddr3.d41,fpga.D26, hw.ddr3.d42,fpga.C25, hw.ddr3.d43,fpga.E23, hw.ddr3.d44,fpga.D22, hw.ddr3.d45,fpga.F22, hw.ddr3.d46,fpga.E22, hw.ddr3.d47,fpga.A30, hw.ddr3.d48,fpga.D27, hw.ddr3.d49,fpga.A29, hw.ddr3.d50,fpga.C28, hw.ddr3.d51,fpga.D28, hw.ddr3.d52,fpga.B31, hw.ddr3.d53,fpga.A31, hw.ddr3.d54,fpga.A32, hw.ddr3.d55,fpga.E30, hw.ddr3.d56,fpga.F29, hw.ddr3.d57,fpga.F30, hw.ddr3.d58,fpga.F27, hw.ddr3.d59,fpga.C30, hw.ddr3.d60,fpga.E29, hw.ddr3.d61,fpga.F26, hw.ddr3.d62,fpga.D30, hw.ddr3.d63,fpga.N16, hw.ddr3.dqs0_p,fpga.M16, hw.ddr3.dqs0_n,fpga.K12, hw.ddr3.dqs1_p,fpga.J12, hw.ddr3.dqs1_n,fpga.H16, hw.ddr3.dqs2_p,fpga.G16, hw.ddr3.dqs2_n,fpga.C15, hw.ddr3.dqs3_p,fpga.C14, hw.ddr3.dqs3_n,fpga.A26, hw.ddr3.dqs4_p,fpga.A27, hw.ddr3.dqs4_n,fpga.F25, hw.ddr3.dqs5_p,fpga.E25, hw.ddr3.dqs5_n,fpga.B28, hw.ddr3.dqs6_p,fpga.B29, hw.ddr3.dqs6_n,fpga.E27, hw.ddr3.dqs7_p,fpga.E28, hw.ddr3.dqs7_n,fpga.M13, hw.ddr3.dm0,fpga.K15, hw.ddr3.dm1,fpga.F12, hw.ddr3.dm2,fpga.A14, hw.ddr3.dm3,fpga.C23, hw.ddr3.dm4,fpga.D25, hw.ddr3.dm5,fpga.C31, hw.ddr3.dm6,fpga.F31, hw.ddr3.dm7,fpga.A20, hw.ddr3.a0,fpga.B19, hw.ddr3.a1,fpga.C20, hw.ddr3.a2,fpga.A19, hw.ddr3.a3,fpga.A17, hw.ddr3.a4,fpga.A16, hw.ddr3.a5,fpga.D20, hw.ddr3.a6,fpga.C18, hw.ddr3.a7,fpga.D17, hw.ddr3.a8,fpga.C19, hw.ddr3.a9,fpga.B21, hw.ddr3.a10,fpga.B17, hw.ddr3.a11,fpga.A15, hw.ddr3.a12,fpga.A21, hw.ddr3.a13,fpga.F17, hw.ddr3.a14,fpga.E17, hw.ddr3.a15,fpga.D21, hw.ddr3.ba0,fpga.C21, hw.ddr3.ba1,fpga.D18, hw.ddr3.ba2,fpga.J17, hw.ddr3.s0_b,fpga.J20, hw.ddr3.s1_b}
);
// ethernet
via viaphymdio (fpga.AK33, hw.phy_mdio);
//assign fpga.AJ33=hw.phy_reset;
//assign fpga.AH31= hw.phy_mdc;
OBUF mdioreset(.O(fpga.AJ33),.I(hw.phy_reset));
OBUF mdiomdc(.O(fpga.AH31),.I(hw.phy_mdc));
IBUF mdioint(.O(hw.phy_int),.I(fpga.AL31));
//obufbus #(.WIDTH(3))
//obufphy(.O({fpga.AJ33,fpga.AH31,fpga.AL31}),.I({hw.phy_reset,hw.phy_mdc,hw.phy_int}));
//assign hw.sgmiiclk_q0_p=fpga.AH8;
//assign hw.sgmiiclk_q0_n=fpga.AH7;
assign hw.sgmii_rx_p=fpga.AM8;
assign hw.sgmii_rx_n=fpga.AM7;
assign fpga.AN2= hw.sgmii_tx_p;
assign fpga.AN1= hw.sgmii_tx_n;
//viabus #(.WIDTH(4))
//viasgmii ({hw.sgmii_rx_p,hw.sgmii_rx_n,hw.sgmii_tx_p,hw.sgmii_tx_n},{fpga.AM8,fpga.AM7,fpga.AN2,fpga.AN1});
//end
//endgenerate
//fan
//assign fpga.BA37=hw.sm_fan_pwm;
//assign hw.sm_fan_tach=fpga.BB37;
obufbus #(.WIDTH(1))
obuffan (.O(fpga.BA37),.I(hw.sm_fan_pwm));
ibufbus #(.WIDTH(1))
ibuffan (.O(hw.sm_fan_tach),.I(fpga.BB37));
//flash
viapairs #(.WIDTH(48))
flash_via ({fpga.AM36,hw.flash.d0,fpga.AN36, hw.flash.d1,fpga.AJ36, hw.flash.d2,fpga.AJ37, hw.flash.d3,fpga.AK37, hw.flash.d4,fpga.AL37, hw.flash.d5,fpga.AN35, hw.flash.d6,fpga.AP35, hw.flash.d7,fpga.AM37, hw.flash.d8,fpga.AG33, hw.flash.d9,fpga.AH33, hw.flash.d10,fpga.AK35, hw.flash.d11,fpga.AL35, hw.flash.d12,fpga.AJ31, hw.flash.d13,fpga.AH34, hw.flash.d14,fpga.AJ35, hw.flash.d15,fpga.AL36, hw.flash.ce_b,fpga.AM34, hw.flash.wait_,fpga.AJ28, hw.flash.a0,fpga.AH28, hw.flash.a1,fpga.AG31, hw.flash.a2,fpga.AF30, hw.flash.a3,fpga.AK29, hw.flash.a4,fpga.AK28, hw.flash.a5,fpga.AG29, hw.flash.a6,fpga.AK30, hw.flash.a7,fpga.AJ30, hw.flash.a8,fpga.AH30, hw.flash.a9,fpga.AH29, hw.flash.a10,fpga.AL30, hw.flash.a11,fpga.AL29, hw.flash.a12,fpga.AN33, hw.flash.a13,fpga.AM33, hw.flash.a14,fpga.AM32, hw.flash.a15,fpga.AV41, hw.flash.a16,fpga.AU41, hw.flash.a17,fpga.BA42, hw.flash.a18,fpga.AU42, hw.flash.a19,fpga.AT41, hw.flash.a20,fpga.BA40, hw.flash.a21,fpga.BA39, hw.flash.a22,fpga.BB39, hw.flash.a23,fpga.AW42, hw.flash.a24,fpga.AW41, hw.flash.a25,fpga.AY37, hw.flash.adv_b,fpga.BA41, hw.flash.oe_b,fpga.BB41, hw.flash.fwe_b,fpga.AP37, hw.flash.fpga_emcclk}
);
//hdmi
viapairs #(.WIDTH(43))
hdmi_via ({fpga.AM24,hw.hdmi.int_,fpga.AP21, hw.hdmi.de,fpga.AR23, hw.hdmi.spdif,fpga.AR22, hw.hdmi.spdif_out,fpga.AT22, hw.hdmi.vsync,fpga.AU22, hw.hdmi.hsync,fpga.AU23, hw.hdmi.clk,fpga.AM22, hw.hdmi.d0,fpga.AL22, hw.hdmi.d1,fpga.AJ20, hw.hdmi.d2,fpga.AJ21, hw.hdmi.d3,fpga.AM21, hw.hdmi.d4,fpga.AL21, hw.hdmi.d5,fpga.AK22, hw.hdmi.d6,fpga.AJ22, hw.hdmi.d7,fpga.AL20, hw.hdmi.d8,fpga.AK20, hw.hdmi.d9,fpga.AK23, hw.hdmi.d10,fpga.AJ23, hw.hdmi.d11,fpga.AN21, hw.hdmi.d12,fpga.AP22, hw.hdmi.d13,fpga.AP23, hw.hdmi.d14,fpga.AN23, hw.hdmi.d15,fpga.AM23, hw.hdmi.d16,fpga.AN24, hw.hdmi.d17,fpga.AY24, hw.hdmi.d18,fpga.BB22, hw.hdmi.d19,fpga.BA22, hw.hdmi.d20,fpga.BA25, hw.hdmi.d21,fpga.AY25, hw.hdmi.d22,fpga.AY22, hw.hdmi.d23,fpga.AY23, hw.hdmi.d24,fpga.AV24, hw.hdmi.d25,fpga.AU24, hw.hdmi.d26,fpga.AW21, hw.hdmi.d27,fpga.AV21, hw.hdmi.d28,fpga.AT24, hw.hdmi.d29,fpga.AR24, hw.hdmi.d30,fpga.AU21, hw.hdmi.d31,fpga.AT21, hw.hdmi.d32,fpga.AW22, hw.hdmi.d33,fpga.AW23, hw.hdmi.d34,fpga.AV23, hw.hdmi.d35}
);
//iic
viapairs #(.WIDTH(2)) iic_via ({fpga.AT35,hw.iic.scl, fpga.AU32, hw.iic.sda});
//assign fpga.AY42=hw.iic.mux_reset_b;
OBUF muxresetb(.I(hw.iic.mux_reset_b),.O(fpga.AY42));

//lcd
viapairs #(.WIDTH(7))
lcd_via ({fpga.AN41,hw.lcd.rs,fpga.AT40,hw.lcd.e,fpga.AR42,hw.lcd.rw,fpga.AT42,hw.lcd.db4,fpga.AR38,hw.lcd.db5,fpga.AR39,hw.lcd.db6,fpga.AN40,hw.lcd.db7}
);
//pcie
if (USE_PCIE_MGT=="TRUE")
viapairs #(.WIDTH(36))
pcie_via ({fpga.AV33,hw.pcie.B11
,fpga.AV35,hw.pcie.A11
,fpga.AG2,hw.pcie.A35
,fpga.AD4,hw.pcie.B33
,fpga.AG1,hw.pcie.A36
,fpga.AD3,hw.pcie.B34
,fpga.AH4,hw.pcie.A39
,fpga.AE6,hw.pcie.B37
,fpga.AH3,hw.pcie.A40
,fpga.AE5,hw.pcie.B38
,fpga.AJ2,hw.pcie.A43
,fpga.AF4,hw.pcie.B41
,fpga.AJ1,hw.pcie.A44
,fpga.AF3,hw.pcie.B42
,fpga.AK4,hw.pcie.A47
,fpga.AG6,hw.pcie.B45
,fpga.AK3,hw.pcie.A48
,fpga.AG5,hw.pcie.B46
,fpga.W2,hw.pcie.A16
,fpga.Y4,hw.pcie.B14
,fpga.W1,hw.pcie.A17
,fpga.Y3,hw.pcie.B15
,fpga.AA2,hw.pcie.A21
,fpga.AA6,hw.pcie.B19
,fpga.AA1,hw.pcie.A22
,fpga.AA5,hw.pcie.B20
,fpga.AB7,hw.pcie.A14
,fpga.AB8,hw.pcie.A13
,fpga.AC2,hw.pcie.A25
,fpga.AB4,hw.pcie.B23
,fpga.AC1,hw.pcie.A26
,fpga.AB3,hw.pcie.B24
,fpga.AE2,hw.pcie.A29
,fpga.AC6,hw.pcie.B27
,fpga.AE1,hw.pcie.A30
,fpga.AC5,hw.pcie.B28}
);
else
viapairs #(.WIDTH(2))
pcie_via ({fpga.AV33,hw.pcie.B11
,fpga.AV35,hw.pcie.A11
});
//viapairs #(.WIDTH(36))
//pcie_via ({fpga.AV33,hw.pcie.wake_b,fpga.AV35,hw.pcie.perst,fpga.AG2,hw.pcie.tx4_p,fpga.AD4,hw.pcie.rx4_p,fpga.AG1,hw.pcie.tx4_n,fpga.AD3,hw.pcie.rx4_n,fpga.AH4,hw.pcie.tx5_p,fpga.AE6,hw.pcie.rx5_p,fpga.AH3,hw.pcie.tx5_n,fpga.AE5,hw.pcie.rx5_n,fpga.AJ2,hw.pcie.tx6_p,fpga.AF4,hw.pcie.rx6_p,fpga.AJ1,hw.pcie.tx6_n,fpga.AF3,hw.pcie.rx6_n,fpga.AK4,hw.pcie.tx7_p,fpga.AG6,hw.pcie.rx7_p,fpga.AK3,hw.pcie.tx7_n,fpga.AG5,hw.pcie.rx7_n,fpga.W2,hw.pcie.tx0_p,fpga.Y4,hw.pcie.rx0_p,fpga.W1,hw.pcie.tx0_n,fpga.Y3,hw.pcie.rx0_n,fpga.AA2,hw.pcie.tx1_p,fpga.AA6,hw.pcie.rx1_p,fpga.AA1,hw.pcie.tx1_n,fpga.AA5,hw.pcie.rx1_n,fpga.AB7,hw.pcie.clk_qo_n,fpga.AB8,hw.pcie.clk_qo_p,fpga.AC2,hw.pcie.tx2_p,fpga.AB4,hw.pcie.rx2_p,fpga.AC1,hw.pcie.tx2_n,fpga.AB3,hw.pcie.rx2_n,fpga.AE2,hw.pcie.tx3_p,fpga.AC6,hw.pcie.rx3_p,fpga.AE1,hw.pcie.tx3_n,fpga.AC5,hw.pcie.rx3_n}
//);
//pmbus
viapairs #(.WIDTH(3))
pmbus_via ({fpga.AV38,hw.pmbus_alert, fpga.AY39, hw.pmbus_data, fpga.AW37, hw.pmbus_clk}
);
//sdio
viapairs #(.WIDTH(8))
sdio_via ({fpga.AR30,hw.sdio.dat0,fpga.AT30,hw.sdio.cd_dat3,fpga.AU31,hw.sdio.dat1,fpga.AV31,hw.sdio.dat2,fpga.AN30,hw.sdio.clk,fpga.AP30,hw.sdio.cmd,fpga.AP32,hw.sdio.sddet,fpga.AR32,hw.sdio.sdwp}
);
//sfp
//assign fpga.AP33=hw.sfp.tx_disable;
//assign hw.sfp.los=fpga.BB38;
obufbus #(.WIDTH(1))
obufsfp (.O(fpga.AP33),.I(hw.sfp.tx_disable));
ibufbus #(.WIDTH(1))
ibufsfp (.O(hw.sfp.los),.I(fpga.BB38));
//generate if (USE_SFP_MGT=="TRUE") begin
//	assign fpga.AM4=hw.sfp.tx_p;
//	assign fpga.AM3=hw.sfp.tx_n;
viapairs #(.WIDTH(4))
viasfp({fpga.AM4,hw.sfp.tx_p
,fpga.AM3,hw.sfp.tx_n
,hw.sfp.rx_p,fpga.AL6
,hw.sfp.rx_n,fpga.AL5
});
//end
//endgenerate
//assign hw.sfp.rx_p=fpga.AL6;
//ssign hw.sfp.rx_n=fpga.AL5;
//usb2uart
//assign hw.usb2uart.rx=fpga.AU33;
//assign fpga.AU36=hw.usb2uart.tx;
obufbus #(.WIDTH(1))
obufuart (.O(fpga.AU36),.I(hw.usb2uart.tx));
ibufbus #(.WIDTH(1))
ibufuart (.O(hw.usb2uart.rx),.I(fpga.AU33));
viapairs #(.WIDTH(2))
viauart ({fpga.AT32, hw.usb2uart.rts, fpga.AR34, hw.usb2uart.cts});
//usbsmsc
viapairs #(.WIDTH(14))
usb_via ({fpga.BA35,hw.usbsmsc.nxt,fpga.BB36,hw.usbsmsc.reset_b,fpga.BB32,hw.usbsmsc.stp,fpga.BB33,hw.usbsmsc.dir,fpga.AV34,hw.usbsmsc.refclk_option,fpga.AY32,hw.usbsmsc.clkout,fpga.AV36,hw.usbsmsc.data0,fpga.AW36,hw.usbsmsc.data1,fpga.BA34,hw.usbsmsc.data2,fpga.BB34,hw.usbsmsc.data3,fpga.BA36,hw.usbsmsc.data4,fpga.AT34,hw.usbsmsc.data5,fpga.AY35,hw.usbsmsc.data6,fpga.AW35,hw.usbsmsc.data7}
);

//xadc
viapairs #(.WIDTH(8))
xadc_via ({fpga.AN38,hw.xadc.vaux0p_r,fpga.AP38,hw.xadc.vaux0n_r,fpga.AM41,hw.xadc.vaux8p_r,fpga.AM42,hw.xadc.vaux8n_r,fpga.BA21,hw.xadc.gpio_0,fpga.BB21,hw.xadc.gpio_1,fpga.BB24,hw.xadc.gpio_2,fpga.BB23,hw.xadc.gpio_3}
);
// gpio
//assign {hw.rotary_push, hw.rotary_inca, hw.rotary_incb}={fpga.AW31, fpga.AR33, fpga.AT31};
ibufbus #(.WIDTH(3))
ibufrotary (.O({hw.rotary_push, hw.rotary_inca, hw.rotary_incb}),.I({fpga.AW31, fpga.AR33, fpga.AT31}));
//assign {hw.cpu_reset, hw.gpio_sw_s, hw.gpio_sw_n, hw.gpio_sw_c, hw.gpio_sw_e, hw.gpio_sw_w}={fpga.AV40, fpga.AP40, fpga.AR40, fpga.AV39, fpga.AU38, fpga.AW40};
ibufbus #(.WIDTH(6))
ibufgpiosw(.O({hw.cpu_reset, hw.gpio_sw_s, hw.gpio_sw_n, hw.gpio_sw_c, hw.gpio_sw_e, hw.gpio_sw_w}),.I({fpga.AV40, fpga.AP40, fpga.AR40, fpga.AV39, fpga.AU38, fpga.AW40}));

//assign {hw.gpio_dip_sw0, hw.gpio_dip_sw1, hw.gpio_dip_sw2, hw.gpio_dip_sw3, hw.gpio_dip_sw4, hw.gpio_dip_sw5, hw.gpio_dip_sw6, hw.gpio_dip_sw7}={fpga.AV30, fpga.AY33, fpga.BA31, fpga.BA32, fpga.AW30, fpga.AY30, fpga.BA30, fpga.BB31};
ibufbus #(.WIDTH(8))
ibufgpiodip(.O({hw.gpio_dip_sw0, hw.gpio_dip_sw1, hw.gpio_dip_sw2, hw.gpio_dip_sw3, hw.gpio_dip_sw4, hw.gpio_dip_sw5, hw.gpio_dip_sw6, hw.gpio_dip_sw7}),.I({fpga.AV30, fpga.AY33, fpga.BA31, fpga.BA32, fpga.AW30, fpga.AY30, fpga.BA30, fpga.BB31}));
//assign {fpga.AM39,fpga.AN39,fpga.AR37,fpga.AT37,fpga.AR35,fpga.AP41,fpga.AP42,fpga.AU39}={hw.gpio_led_0,hw.gpio_led_1,hw.gpio_led_2,hw.gpio_led_3,hw.gpio_led_4,hw.gpio_led_5,hw.gpio_led_6,hw.gpio_led_7};
obufbus #(.WIDTH(8))
obufgpioled(.O({fpga.AM39,fpga.AN39,fpga.AR37,fpga.AT37,fpga.AR35,fpga.AP41,fpga.AP42,fpga.AU39}),.I({hw.gpio_led_0,hw.gpio_led_1,hw.gpio_led_2,hw.gpio_led_3,hw.gpio_led_4,hw.gpio_led_5,hw.gpio_led_6,hw.gpio_led_7}));

viapairs #(.WIDTH(6))
sma_via ({fpga.AN31,hw.user_sma_gpio_p, fpga.AP31, hw.user_sma_gpio_n, fpga.AJ32, hw.user_sma_clock_p, fpga.AK32, hw.user_sma_clock_n, fpga.AK34, hw.user_clock_p, fpga.AL34, hw.user_clock_n}
);

via viafmcvadj(hw.fmc_vadj_on_b_ls,fpga.AH35);
//fmc1
viapairs #(.WIDTH(203))
//viapairs #(.WIDTH(203-16))
fmc1via({
hw.fmc1pin.A10,fpga.A6,
hw.fmc1pin.A11,fpga.A5,
hw.fmc1pin.A14,fpga.H8,
hw.fmc1pin.A15,fpga.H7,
hw.fmc1pin.A18,fpga.G6,
hw.fmc1pin.A19,fpga.G5,
hw.fmc1pin.A2,fpga.C6,
hw.fmc1pin.A3,fpga.C5,

hw.fmc1pin.A22,fpga.D4, // gtio
hw.fmc1pin.A23,fpga.D3, // gtio
hw.fmc1pin.A26,fpga.C2, // gtio
hw.fmc1pin.A27,fpga.C1, // gtio
hw.fmc1pin.A30,fpga.B4, // gtio
hw.fmc1pin.A31,fpga.B3, // gtio
hw.fmc1pin.A34,fpga.J2, // gtio
hw.fmc1pin.A35,fpga.J1, // gtio
hw.fmc1pin.A38,fpga.H4, // gtio
hw.fmc1pin.A39,fpga.H3, // gtio
hw.fmc1pin.B32,fpga.F4, // gtio
hw.fmc1pin.B33,fpga.F3, // gtio
hw.fmc1pin.B36,fpga.G2, // gtio
hw.fmc1pin.B37,fpga.G1, // gtio
hw.fmc1pin.C2,fpga.E2, // gtio
hw.fmc1pin.C3,fpga.E1, // gtio

hw.fmc1pin.A6,fpga.B8,
hw.fmc1pin.A7,fpga.B7,
hw.fmc1pin.B12,fpga.E6,
hw.fmc1pin.B13,fpga.E5,
hw.fmc1pin.B16,fpga.F8,
hw.fmc1pin.B17,fpga.F7,
hw.fmc1pin.B20,fpga.E10,
hw.fmc1pin.B21,fpga.E9,
hw.fmc1pin.C10,fpga.K42,
hw.fmc1pin.C11,fpga.J42,
hw.fmc1pin.C14,fpga.N38,
hw.fmc1pin.C15,fpga.M39,
hw.fmc1pin.C18,fpga.N39,
hw.fmc1pin.C19,fpga.N40,
hw.fmc1pin.C22,fpga.M32,
hw.fmc1pin.C23,fpga.L32,
hw.fmc1pin.C26,fpga.J31,
hw.fmc1pin.C27,fpga.H31,
hw.fmc1pin.C6,fpga.D8,
hw.fmc1pin.C7,fpga.D7,
hw.fmc1pin.D1,fpga.AL32,
hw.fmc1pin.D11,fpga.M41,
hw.fmc1pin.D12,fpga.L41,
hw.fmc1pin.D14,fpga.R42,
hw.fmc1pin.D15,fpga.P42,
hw.fmc1pin.D17,fpga.H39,
hw.fmc1pin.D18,fpga.G39,
hw.fmc1pin.D20,fpga.L31,
hw.fmc1pin.D21,fpga.K32,
hw.fmc1pin.D23,fpga.P30,
hw.fmc1pin.D24,fpga.N31,
hw.fmc1pin.D26,fpga.J30,
hw.fmc1pin.D27,fpga.H30,
hw.fmc1pin.D4,fpga.A10,
hw.fmc1pin.D5,fpga.A9,
hw.fmc1pin.D8,fpga.J40,
hw.fmc1pin.D9,fpga.J41,
hw.fmc1pin.E10,fpga.D32,
hw.fmc1pin.E12,fpga.B36,
hw.fmc1pin.E13,fpga.A37,
hw.fmc1pin.E15,fpga.B39,
hw.fmc1pin.E16,fpga.A39,
hw.fmc1pin.E18,fpga.B34,
hw.fmc1pin.E19,fpga.A34,
hw.fmc1pin.E2,fpga.D35,
hw.fmc1pin.E21,fpga.G28,
hw.fmc1pin.E22,fpga.G29,
hw.fmc1pin.E24,fpga.K27,
hw.fmc1pin.E25,fpga.J27,
hw.fmc1pin.E27,fpga.H23,
hw.fmc1pin.E28,fpga.G23,
hw.fmc1pin.E3,fpga.D36,
hw.fmc1pin.E30,fpga.P25,
hw.fmc1pin.E31,fpga.P26,
hw.fmc1pin.E33,fpga.L25,
hw.fmc1pin.E34,fpga.L26,
hw.fmc1pin.E36,fpga.P22,
hw.fmc1pin.E37,fpga.P23,
hw.fmc1pin.E6,fpga.G32,
hw.fmc1pin.E7,fpga.F32,
hw.fmc1pin.E9,fpga.E32,
hw.fmc1pin.F1,fpga.AN34,
hw.fmc1pin.F10,fpga.J36,
hw.fmc1pin.F11,fpga.H36,
hw.fmc1pin.F13,fpga.B37,
hw.fmc1pin.F14,fpga.B38,
hw.fmc1pin.F16,fpga.C33,
hw.fmc1pin.F17,fpga.C34,
hw.fmc1pin.F19,fpga.B32,
hw.fmc1pin.F20,fpga.B33,
hw.fmc1pin.F22,fpga.K28,
hw.fmc1pin.F23,fpga.J28,
hw.fmc1pin.F25,fpga.H24,
hw.fmc1pin.F26,fpga.G24,
hw.fmc1pin.F28,fpga.H25,
hw.fmc1pin.F29,fpga.H26,
hw.fmc1pin.F31,fpga.K24,
hw.fmc1pin.F32,fpga.K25,
hw.fmc1pin.F34,fpga.N25,
hw.fmc1pin.F35,fpga.N26,
hw.fmc1pin.F37,fpga.P21,
hw.fmc1pin.F38,fpga.N21,
hw.fmc1pin.F4,fpga.E34,
hw.fmc1pin.F5,fpga.E35,
hw.fmc1pin.F7,fpga.F34,
hw.fmc1pin.F8,fpga.F35,
hw.fmc1pin.G10,fpga.L42,
hw.fmc1pin.G12,fpga.M37,
hw.fmc1pin.G13,fpga.M38,
hw.fmc1pin.G15,fpga.R40,
hw.fmc1pin.G16,fpga.P40,
hw.fmc1pin.G18,fpga.K37,
hw.fmc1pin.G19,fpga.K38,
hw.fmc1pin.G2,fpga.N30,
hw.fmc1pin.G21,fpga.Y29,
hw.fmc1pin.G22,fpga.Y30,
hw.fmc1pin.G24,fpga.R28,
hw.fmc1pin.G25,fpga.P28,
hw.fmc1pin.G27,fpga.K29,
hw.fmc1pin.G28,fpga.K30,
hw.fmc1pin.G3,fpga.M31,
hw.fmc1pin.G30,fpga.T29,
hw.fmc1pin.G31,fpga.T30,
hw.fmc1pin.G33,fpga.M28,
hw.fmc1pin.G34,fpga.M29,
hw.fmc1pin.G36,fpga.U31,
hw.fmc1pin.G37,fpga.T31,
hw.fmc1pin.G6,fpga.K39,
hw.fmc1pin.G7,fpga.K40,
hw.fmc1pin.G9,fpga.M42,
hw.fmc1pin.H10,fpga.H40,
hw.fmc1pin.H11,fpga.H41,
hw.fmc1pin.H13,fpga.G41,
hw.fmc1pin.H14,fpga.G42,
hw.fmc1pin.H16,fpga.F40,
hw.fmc1pin.H17,fpga.F41,
hw.fmc1pin.H19,fpga.M36,
hw.fmc1pin.H2,fpga.AM31,
hw.fmc1pin.H20,fpga.L37,
hw.fmc1pin.H22,fpga.W30,
hw.fmc1pin.H23,fpga.W31,
hw.fmc1pin.H25,fpga.N28,
hw.fmc1pin.H26,fpga.N29,
hw.fmc1pin.H28,fpga.R30,
hw.fmc1pin.H29,fpga.P31,
hw.fmc1pin.H31,fpga.L29,
hw.fmc1pin.H32,fpga.L30,
hw.fmc1pin.H34,fpga.V30,
hw.fmc1pin.H35,fpga.V31,
hw.fmc1pin.H37,fpga.V29,
hw.fmc1pin.H38,fpga.U29,
hw.fmc1pin.H4,fpga.L39,
hw.fmc1pin.H5,fpga.L40,
hw.fmc1pin.H7,fpga.P41,
hw.fmc1pin.H8,fpga.N41,
hw.fmc1pin.J10,fpga.C39,
hw.fmc1pin.J12,fpga.J37,
hw.fmc1pin.J13,fpga.J38,
hw.fmc1pin.J15,fpga.E37,
hw.fmc1pin.J16,fpga.E38,
hw.fmc1pin.J18,fpga.F39,
hw.fmc1pin.J19,fpga.E39,
hw.fmc1pin.J21,fpga.F36,
hw.fmc1pin.J22,fpga.F37,
hw.fmc1pin.J24,fpga.H28,
hw.fmc1pin.J25,fpga.H29,
hw.fmc1pin.J27,fpga.G26,
hw.fmc1pin.J28,fpga.G27,
hw.fmc1pin.J30,fpga.K22,
hw.fmc1pin.J31,fpga.J22,
hw.fmc1pin.J33,fpga.M21,
hw.fmc1pin.J34,fpga.L21,
hw.fmc1pin.J36,fpga.G21,
hw.fmc1pin.J37,fpga.G22,
hw.fmc1pin.J6,fpga.H33,
hw.fmc1pin.J7,fpga.G33,
hw.fmc1pin.J9,fpga.C38,
hw.fmc1pin.K10,fpga.G36,
hw.fmc1pin.K11,fpga.G37,
hw.fmc1pin.K13,fpga.H38,
hw.fmc1pin.K14,fpga.G38,
hw.fmc1pin.K16,fpga.C35,
hw.fmc1pin.K17,fpga.C36,
hw.fmc1pin.K19,fpga.D37,
hw.fmc1pin.K20,fpga.D38,
hw.fmc1pin.K22,fpga.A35,
hw.fmc1pin.K23,fpga.A36,
hw.fmc1pin.K25,fpga.J25,
hw.fmc1pin.K26,fpga.J26,
hw.fmc1pin.K28,fpga.K23,
hw.fmc1pin.K29,fpga.J23,
hw.fmc1pin.K31,fpga.M22,
hw.fmc1pin.K32,fpga.L22,
hw.fmc1pin.K34,fpga.J21,
hw.fmc1pin.K35,fpga.H21,
hw.fmc1pin.K37,fpga.M24,
hw.fmc1pin.K38,fpga.L24,
hw.fmc1pin.K7,fpga.E33,
hw.fmc1pin.K8,fpga.D33}
);
viapairs #(.WIDTH(203-44)) // 44 NC pins xc7vx485tffg1761pkg.txt
//viapairs #(.WIDTH(203-44-16))
fmc2via({fpga.J6,hw.fmc2pin.A10
,fpga.J5,hw.fmc2pin.A11
,fpga.W6,hw.fmc2pin.A14
,fpga.W5,hw.fmc2pin.A15
,fpga.V4,hw.fmc2pin.A18
,fpga.V3,hw.fmc2pin.A19
,fpga.N6,hw.fmc2pin.A2
,fpga.N5,hw.fmc2pin.A3
,fpga.M4,hw.fmc2pin.A22 // gtio
,fpga.M3,hw.fmc2pin.A23 // gtio
,fpga.L2,hw.fmc2pin.A26 // gtio
,fpga.L1,hw.fmc2pin.A27 // gtio
,fpga.K4,hw.fmc2pin.A30 // gtio
,fpga.K3,hw.fmc2pin.A31 // gtio
,fpga.U2,hw.fmc2pin.A34 // gtio
,fpga.U1,hw.fmc2pin.A35 // gtio
,fpga.T4,hw.fmc2pin.A38 // gtio
,fpga.T3,hw.fmc2pin.A39 // gtio
,fpga.P4,hw.fmc2pin.B32 // gtio
,fpga.P3,hw.fmc2pin.B33 // gtio
,fpga.R2,hw.fmc2pin.B36 // gtio
,fpga.R1,hw.fmc2pin.B37 // gtio
,fpga.N2,hw.fmc2pin.C2 // gtio
,fpga.N1,hw.fmc2pin.C3 // gtio
,fpga.L6,hw.fmc2pin.A6
,fpga.L5,hw.fmc2pin.A7
,fpga.R6,hw.fmc2pin.B12
,fpga.R5,hw.fmc2pin.B13
,fpga.U6,hw.fmc2pin.B16
,fpga.U5,hw.fmc2pin.B17
,fpga.T8,hw.fmc2pin.B20
,fpga.T7,hw.fmc2pin.B21
,fpga.AD38,hw.fmc2pin.C10
,fpga.AE38,hw.fmc2pin.C11
,fpga.AB41,hw.fmc2pin.C14
,fpga.AB42,hw.fmc2pin.C15
,fpga.AB38,hw.fmc2pin.C18
,fpga.AB39,hw.fmc2pin.C19
,fpga.U36,hw.fmc2pin.C22
,fpga.T37,hw.fmc2pin.C23
,fpga.P32,hw.fmc2pin.C26
,fpga.P33,hw.fmc2pin.C27
,fpga.P8,hw.fmc2pin.C6
,fpga.P7,hw.fmc2pin.C7
,fpga.AL32,hw.fmc2pin.D1
,fpga.AF42,hw.fmc2pin.D11
,fpga.AG42,hw.fmc2pin.D12
,fpga.AJ38,hw.fmc2pin.D14
,fpga.AK38,hw.fmc2pin.D15
,fpga.W40,hw.fmc2pin.D17
,fpga.Y40,hw.fmc2pin.D18
,fpga.U37,hw.fmc2pin.D20
,fpga.U38,hw.fmc2pin.D21
,fpga.R38,hw.fmc2pin.D23
,fpga.R39,hw.fmc2pin.D24
,fpga.N33,hw.fmc2pin.D26
,fpga.N34,hw.fmc2pin.D27
,fpga.K8,hw.fmc2pin.D4
,fpga.K7,hw.fmc2pin.D5
,fpga.AF41,hw.fmc2pin.D8
,fpga.AG41,hw.fmc2pin.D9
,fpga.AE30,hw.fmc2pin.E10
,fpga.AE32,hw.fmc2pin.E12
,fpga.AE33,hw.fmc2pin.E13
,fpga.AG36,hw.fmc2pin.E15
,fpga.AH36,hw.fmc2pin.E16
,fpga.AD36,hw.fmc2pin.E18
,fpga.AD37,hw.fmc2pin.E19
,fpga.AD32,hw.fmc2pin.E2
,fpga.AD33,hw.fmc2pin.E3
,fpga.Y32,hw.fmc2pin.E6
,fpga.Y33,hw.fmc2pin.E7
,fpga.AE29,hw.fmc2pin.E9
///////
//NC,fpga.AT16,hw.fmc2pin.E21
//NC,fpga.AU16,hw.fmc2pin.E22
//NC,fpga.BA17,hw.fmc2pin.E24
//NC,fpga.BB17,hw.fmc2pin.E25
//NC,fpga.AV20,hw.fmc2pin.E27
//NC,fpga.AW20,hw.fmc2pin.E28
//NC,fpga.AT20,hw.fmc2pin.E30
//NC,fpga.AT19,hw.fmc2pin.E31
//NC,fpga.AT18,hw.fmc2pin.E33
//NC,fpga.AP17,hw.fmc2pin.E34
//NC,fpga.AN19,hw.fmc2pin.E36
//NC,fpga.AN18,hw.fmc2pin.E37
///////
,fpga.AF29,hw.fmc2pin.F1
,fpga.AA31,hw.fmc2pin.F10
,fpga.AA32,hw.fmc2pin.F11
,fpga.AF34,hw.fmc2pin.F13
,fpga.AG34,hw.fmc2pin.F14
,fpga.AE37,hw.fmc2pin.F16
,fpga.AF37,hw.fmc2pin.F17
,fpga.AC35,hw.fmc2pin.F19
,fpga.AC36,hw.fmc2pin.F20
///////
//NC,fpga.AV16,hw.fmc2pin.F22
//NC,fpga.AW16,hw.fmc2pin.F23
//NC,fpga.AU18,hw.fmc2pin.F25
//NC,fpga.AV18,hw.fmc2pin.F26
//NC,fpga.AY20,hw.fmc2pin.F28
//NC,fpga.BA20,hw.fmc2pin.F29
//NC,fpga.AU19,hw.fmc2pin.F31
//NC,fpga.AV19,hw.fmc2pin.F32
//NC,fpga.AR18,hw.fmc2pin.F34
//NC,fpga.AR17,hw.fmc2pin.F35
//NC,fpga.AK17,hw.fmc2pin.F37
//NC,fpga.AL17,hw.fmc2pin.F38
///////
,fpga.AB33,hw.fmc2pin.F4
,fpga.AC33,hw.fmc2pin.F5
,fpga.AB29,hw.fmc2pin.F7
,fpga.AC29,hw.fmc2pin.F8
,fpga.AK42,hw.fmc2pin.G10
,fpga.AD42,hw.fmc2pin.G12
,fpga.AE42,hw.fmc2pin.G13
,fpga.Y39,hw.fmc2pin.G15
,fpga.AA39,hw.fmc2pin.G16
,fpga.AJ40,hw.fmc2pin.G18
,fpga.AJ41,hw.fmc2pin.G19
,fpga.U39,hw.fmc2pin.G2
,fpga.V33,hw.fmc2pin.G21
,fpga.V34,hw.fmc2pin.G22
,fpga.W32,hw.fmc2pin.G24
,fpga.W33,hw.fmc2pin.G25
,fpga.R33,hw.fmc2pin.G27
,fpga.R34,hw.fmc2pin.G28
,fpga.T39,hw.fmc2pin.G3
,fpga.W36,hw.fmc2pin.G30
,fpga.W37,hw.fmc2pin.G31
,fpga.V39,hw.fmc2pin.G33
,fpga.V40,hw.fmc2pin.G34
,fpga.T36,hw.fmc2pin.G36
,fpga.R37,hw.fmc2pin.G37
,fpga.AD40,hw.fmc2pin.G6
,fpga.AD41,hw.fmc2pin.G7
,fpga.AJ42,hw.fmc2pin.G9
,fpga.AL41,hw.fmc2pin.H10
,fpga.AL42,hw.fmc2pin.H11
,fpga.AC40,hw.fmc2pin.H13
,fpga.AC41,hw.fmc2pin.H14
,fpga.Y42,hw.fmc2pin.H16
,fpga.AA42,hw.fmc2pin.H17
,fpga.AC38,hw.fmc2pin.H19
,fpga.AG32,hw.fmc2pin.H2
,fpga.AF39,hw.fmc2pin.H4
,fpga.AF40,hw.fmc2pin.H5
,fpga.AK39,hw.fmc2pin.H7
,fpga.AL39,hw.fmc2pin.H8
,fpga.AC39,hw.fmc2pin.H20
,fpga.U32,hw.fmc2pin.H22
,fpga.U33,hw.fmc2pin.H23
,fpga.P35,hw.fmc2pin.H25
,fpga.P36,hw.fmc2pin.H26
,fpga.U34,hw.fmc2pin.H28
,fpga.T35,hw.fmc2pin.H29
,fpga.V35,hw.fmc2pin.H31
,fpga.V36,hw.fmc2pin.H32
,fpga.T32,hw.fmc2pin.H34
,fpga.R32,hw.fmc2pin.H35
,fpga.P37,hw.fmc2pin.H37
,fpga.P38,hw.fmc2pin.H38
,fpga.AD31,hw.fmc2pin.J10
,fpga.AE34,hw.fmc2pin.J12
,fpga.AE35,hw.fmc2pin.J13
,fpga.AF35,hw.fmc2pin.J15
,fpga.AF36,hw.fmc2pin.J16
,fpga.AB36,hw.fmc2pin.J18
,fpga.AB37,hw.fmc2pin.J19
,fpga.Y35,hw.fmc2pin.J21
,fpga.AA36,hw.fmc2pin.J22
//////
//NC,fpga.AM16,hw.fmc2pin.J24
//NC,fpga.AN16,hw.fmc2pin.J25
//NC,fpga.BB19,hw.fmc2pin.J27
//NC,fpga.BB18,hw.fmc2pin.J28
//NC,fpga.AM18,hw.fmc2pin.J30
//NC,fpga.AM17,hw.fmc2pin.J31
//NC,fpga.AL19,hw.fmc2pin.J33
//NC,fpga.AM19,hw.fmc2pin.J34
//NC,fpga.AJ18,hw.fmc2pin.J36
//NC,fpga.AJ17,hw.fmc2pin.J37
//////////
,fpga.AA29,hw.fmc2pin.J6
,fpga.AA30,hw.fmc2pin.J7
,fpga.AC31,hw.fmc2pin.J9
,fpga.AB31,hw.fmc2pin.K10
,fpga.AB32,hw.fmc2pin.K11
,fpga.AF31,hw.fmc2pin.K13
,fpga.AF32,hw.fmc2pin.K14
,fpga.AC34,hw.fmc2pin.K16
,fpga.AD35,hw.fmc2pin.K17
,fpga.AA34,hw.fmc2pin.K19
,fpga.AA35,hw.fmc2pin.K20
,fpga.Y37,hw.fmc2pin.K22
,fpga.AA37,hw.fmc2pin.K23
///////
//NC,fpga.AT17,hw.fmc2pin.K25
//NC,fpga.AU17,hw.fmc2pin.K26
//NC,fpga.AY18,hw.fmc2pin.K28
//NC,fpga.AY17,hw.fmc2pin.K29
//NC,fpga.AP20,hw.fmc2pin.K31
//NC,fpga.AR19,hw.fmc2pin.K32
//NC,fpga.AK19,hw.fmc2pin.K34
//NC,fpga.AK18,hw.fmc2pin.K35
//NC,fpga.AW18,hw.fmc2pin.K37
//NC,fpga.AW17,hw.fmc2pin.K38
///////
,fpga.AC30,hw.fmc2pin.K7
,fpga.AD30,hw.fmc2pin.K8}
);

endmodule
interface ddr3();
wire reset_b,clk1_p,clk1_n,clk0_p,clk0_n,cke0,cke1,we_b,ras_b,cas_b,odt0,odt1,temp_event,d0,d1,d2,d3,d4,d5,d6,d7,d8,d9,d10,d11,d12,d13,d14,d15,d16,d17,d18,d19,d20,d21,d22,d23,d24,d25,d26,d27,d28,d29,d30,d31,d32,d33,d34,d35,d36,d37,d38,d39,d40,d41,d42,d43,d44,d45,d46,d47,d48,d49,d50,d51,d52,d53,d54,d55,d56,d57,d58,d59,d60,d61,d62,d63,dqs0_p,dqs0_n,dqs1_p,dqs1_n,dqs2_p,dqs2_n,dqs3_p,dqs3_n,dqs4_p,dqs4_n,dqs5_p,dqs5_n,dqs6_p,dqs6_n,dqs7_p,dqs7_n,dm0,dm1,dm2,dm3,dm4,dm5,dm6,dm7,a0,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,a13,a14,a15,ba0,ba1,ba2,s0_b,s1_b;
endinterface
interface ifmchpc();
wire clk0_m2c_p,clk0_m2c_n,pg_m2c,prsnt_m2c_b,clk1_m2c_p,clk1_m2c_n,fmc_c2m_pg,fmc_vadj_on_b,dp0_c2m_p,dp0_m2c_p,dp0_c2m_n,dp0_m2c_n,dp1_c2m_p,dp1_m2c_p,dp1_c2m_n,dp1_m2c_n,dp2_c2m_p,dp2_m2c_p,dp2_c2m_n,dp2_m2c_n,dp3_c2m_p,dp3_m2c_p,dp3_c2m_n,dp3_m2c_n,dp4_c2m_p,dp4_m2c_p,dp4_c2m_n,dp4_m2c_n,dp5_c2m_p,dp5_m2c_p,dp5_c2m_n,dp5_m2c_n,dp6_c2m_p,dp6_m2c_p,dp6_c2m_n,dp6_m2c_n,dp7_c2m_p,dp7_m2c_p,dp7_c2m_n,dp7_m2c_n,la00_cc_p,la00_cc_n,la01_cc_p,la01_cc_n,la02_p,la02_n,la03_p,la03_n,la04_p,la04_n,la05_p,la05_n,la06_p,la06_n,la07_p,la07_n,la08_p,la08_n,la09_p,la09_n,la10_p,la10_n,la11_p,la11_n,la12_p,la12_n,la13_p,la13_n,la14_p,la14_n,la15_p,la15_n,la16_p,la16_n,la17_cc_p,la17_cc_n,la18_cc_p,la18_cc_n,la19_p,la19_n,la20_p,la20_n,la21_p,la21_n,la22_p,la22_n,la23_p,la23_n,la24_p,la24_n,la25_p,la25_n,la26_p,la26_n,la27_p,la27_n,la28_p,la28_n,la29_p,la29_n,la30_p,la30_n,la31_p,la31_n,la32_p,la32_n,la33_p,la33_n,ha00_cc_p,ha00_cc_n,ha01_cc_p,ha01_cc_n,ha02_p,ha02_n,ha03_p,ha03_n,ha04_p,ha04_n,ha05_p,ha05_n,ha06_p,ha06_n,ha07_p,ha07_n,ha08_p,ha08_n,ha09_p,ha09_n,ha10_p,ha10_n,ha11_p,ha11_n,ha12_p,ha12_n,ha13_p,ha13_n,ha14_p,ha14_n,ha15_p,ha15_n,ha16_p,ha16_n,ha17_cc_p,ha17_cc_n,ha18_p,ha18_n,ha19_p,ha19_n,ha20_p,ha20_n,ha21_p,ha21_n,ha22_p,ha22_n,ha23_p,ha23_n,hb00_cc_p,hb00_cc_n,hb01_p,hb01_n,hb02_p,hb02_n,hb03_p,hb03_n,hb04_p,hb04_n,hb05_p,hb05_n,hb06_cc_p,hb06_cc_n,hb07_p,hb07_n,hb08_p,hb08_n,hb09_p,hb09_n,hb10_p,hb10_n,hb11_p,hb11_n,hb12_p,hb12_n,hb13_p,hb13_n,hb14_p,hb14_n,hb15_p,hb15_n,hb16_p,hb16_n,hb17_cc_p,hb17_cc_n,hb18_p,hb18_n,hb19_p,hb19_n,hb20_p,hb20_n,hb21_p,hb21_n;
endinterface
interface flash();
wire d0;wire d1;wire d2;wire d3;wire d4;wire d5;wire d6;wire d7;wire d8;wire d9;wire d10;wire d11;wire d12;wire d13;wire d14;wire d15;wire ce_b;wire wait_;wire a0;wire a1;wire a2;wire a3;wire a4;wire a5;wire a6;wire a7;wire a8;wire a9;wire a10;wire a11;wire a12;wire a13;wire a14;wire a15;wire a16;wire a17;wire a18;wire a19;wire a20;wire a21;wire a22;wire a23;wire a24;wire a25;wire adv_b;wire oe_b;wire fwe_b;wire fpga_emcclk;
endinterface
interface hdmi();
 wire int_; wire de; wire spdif; wire spdif_out; wire vsync; wire hsync; wire clk; wire d0; wire d1; wire d2; wire d3; wire d4; wire d5; wire d6; wire d7; wire d8; wire d9; wire d10; wire d11; wire d12; wire d13; wire d14; wire d15; wire d16; wire d17; wire d18; wire d19; wire d20; wire d21; wire d22; wire d23; wire d24; wire d25; wire d26; wire d27; wire d28; wire d29; wire d30; wire d31; wire d32; wire d33; wire d34; wire d35;
endinterface
interface iic();
wire scl; wire sda; wire mux_reset_b;
modport hw(input scl,mux_reset_b
,inout sda);
endinterface
interface lcd();
 wire rs; wire e; wire rw; wire db4; wire db5; wire db6; wire db7;
endinterface
interface pcie();
wire wake_b; wire perst; wire tx4_p; wire rx4_p; wire tx4_n; wire rx4_n; wire tx5_p; wire rx5_p; wire tx5_n; wire rx5_n; wire tx6_p; wire rx6_p; wire tx6_n; wire rx6_n; wire tx7_p; wire rx7_p; wire tx7_n; wire rx7_n; wire tx0_p; wire rx0_p; wire tx0_n; wire rx0_n; wire tx1_p; wire rx1_p; wire tx1_n; wire rx1_n; wire clk_qo_n; wire clk_qo_p; wire tx2_p; wire rx2_p; wire tx2_n; wire rx2_n; wire tx3_p; wire rx3_p; wire tx3_n; wire rx3_n;
endinterface
interface sdio();
 wire dat0; wire cd_dat3; wire dat1; wire dat2; wire clk; wire cmd; wire sddet; wire sdwp;
endinterface
interface sfp();
 wire tx_disable; wire los; wire tx_p; wire rx_p; wire tx_n; wire rx_n;
endinterface
interface usbsmsc();
wire nxt; wire reset_b; wire stp; wire dir; wire refclk_option; wire clkout; wire data0; wire data1; wire data2; wire data3; wire data4; wire data5; wire data6; wire data7;
endinterface
interface usb2uart();
wire rx; wire tx; wire rts; wire cts;
endinterface
interface xadc();
 wire vaux0p_r; wire vaux0n_r; wire vaux8p_r; wire vaux8n_r; wire gpio_0; wire gpio_1; wire gpio_2; wire gpio_3;
endinterface
interface ifmchpcsig();
wire CLK0_C2M_N,CLK0_C2M_P,CLK0_M2C_N,CLK0_M2C_P,CLK1_C2M_N,CLK1_C2M_P,CLK1_M2C_N,CLK1_M2C_P,DP0_C2M_N,DP0_C2M_P,DP0_M2C_N,DP0_M2C_P,DP1_C2M_N,DP1_C2M_P,DP1_M2C_N,DP1_M2C_P,DP2_C2M_N,DP2_C2M_P,DP2_M2C_N,DP2_M2C_P,DP3_C2M_N,DP3_C2M_P,DP3_M2C_N,DP3_M2C_P,DP4_C2M_N,DP4_C2M_P,DP4_M2C_N,DP4_M2C_P,DP5_C2M_N,DP5_C2M_P,DP5_M2C_N,DP5_M2C_P,DP6_C2M_N,DP6_C2M_P,DP6_M2C_N,DP6_M2C_P,DP7_C2M_N,DP7_C2M_P,DP7_M2C_N,DP7_M2C_P,DP8_C2M_N,DP8_C2M_P,DP8_M2C_N,DP8_M2C_P,DP9_C2M_N,DP9_C2M_P,DP9_M2C_N,DP9_M2C_P,GA0,GA1,GBTCLK0_M2C_N,GBTCLK0_M2C_P,GBTCLK1_M2C_N,GBTCLK1_M2C_P,HA00_N_CC,HA00_P_CC,HA01_N_CC,HA01_P_CC,HA02_N,HA02_P,HA03_N,HA03_P,HA04_N,HA04_P,HA05_N,HA05_P,HA06_N,HA06_P,HA07_N,HA07_P,HA08_N,HA08_P,HA09_N,HA09_P,HA10_N,HA10_P,HA11_N,HA11_P,HA12_N,HA12_P,HA13_N,HA13_P,HA14_N,HA14_P,HA15_N,HA15_P,HA16_N,HA16_P,HA17_N_CC,HA17_P_CC,HA18_N,HA18_P,HA19_N,HA19_P,HA20_N,HA20_P,HA21_N,HA21_P,HA22_N,HA22_P,HA23_N,HA23_P,HB00_N_CC,HB00_P_CC,HB01_N,HB01_P,HB02_N,HB02_P,HB03_N,HB03_P,HB04_N,HB04_P,HB05_N,HB05_P,HB06_N_CC,HB06_P_CC,HB07_N,HB07_P,HB08_N,HB08_P,HB09_N,HB09_P,HB10_N,HB10_P,HB11_N,HB11_P,HB12_N,HB12_P,HB13_N,HB13_P,HB14_N,HB14_P,HB15_N,HB15_P,HB16_N,HB16_P,HB17_N_CC,HB17_P_CC,HB18_N,HB18_P,HB19_N,HB19_P,HB20_N,HB20_P,HB21_N,HB21_P,LA00_N_CC,LA00_P_CC,LA01_N_CC,LA01_P_CC,LA02_N,LA02_P,LA03_N,LA03_P,LA04_N,LA04_P,LA05_N,LA05_P,LA06_N,LA06_P,LA07_N,LA07_P,LA08_N,LA08_P,LA09_N,LA09_P,LA10_N,LA10_P,LA11_N,LA11_P,LA12_N,LA12_P,LA13_N,LA13_P,LA14_N,LA14_P,LA15_N,LA15_P,LA16_N,LA16_P,LA17_N_CC,LA17_P_CC,LA18_N_CC,LA18_P_CC,LA19_N,LA19_P,LA20_N,LA20_P,LA21_N,LA21_P,LA22_N,LA22_P,LA23_N,LA23_P,LA24_N,LA24_P,LA25_N,LA25_P,LA26_N,LA26_P,LA27_N,LA27_P,LA28_N,LA28_P,LA29_N,LA29_P,LA30_N,LA30_P,LA31_N,LA31_P,LA32_N,LA32_P,LA33_N,LA33_P,PG_C2M,PG_M2C,PRSNT_M2C_L,RES0,RES1,SCL,SDA,TCK,TDI,TDO,TMS,TRST_L;
endinterface
interface ifmchpcpin_2();
wire G3,G2,H5,H4,J3,J2,K5,K4,C3,C2,C7,C6,A23,A22,A3,A2,A27,A26,A7,A6,A31,A30,A11,A10,A35,A34,A15,A14,A39,A38,A19,A18,B37,B36,B17,B16,B33,B32,B13,B12,B29,B28,B9,B8,B25,B24,B5,B4,C34,D35,D5,D4,B21,B20,F5,F4,E3,E2,K8,K7,J7,J6,F8,F7,E7,E6,K11,K10,J10,J9,F11,F10,E10,E9,K14,K13,J13,J12,F14,F13,E13,E12,J16,J15,F17,F16,E16,E15,K17,K16,J19,J18,F20,F19,E19,E18,K20,K19,J22,J21,K23,K22,K26,K25,J25,J24,F23,F22,E22,E21,F26,F25,E25,E24,K29,K28,J28,J27,F29,F28,E28,E27,K32,K31,J31,J30,F32,F31,E31,E30,K35,K34,J34,J33,F35,F34,K38,K37,J37,J36,E34,E33,F38,F37,E37,E36,G7,G6,D9,D8,H8,H7,G10,G9,H11,H10,D12,D11,C11,C10,H14,H13,G13,G12,D15,D14,C15,C14,H17,H16,G16,G15,D18,D17,C19,C18,H20,H19,G19,G18,D21,D20,C23,C22,H23,H22,G22,G21,H26,H25,G25,G24,D24,D23,H29,H28,G28,G27,D27,D26,C27,C26,H32,H31,G31,G30,H35,H34,G34,G33,H38,H37,G37,G36,D1,F1,H2,B40,B1,C30,C31,D29,D30,D31,D33,D34;
modport pin(inout
G3,G2,H5,H4,J3,J2,K5,K4,C3,C2,C7,C6,A23,A22,A3,A2,A27,A26,A7,A6,A31,A30,A11,A10,A35,A34,A15,A14,A39,A38,A19,A18,B37,B36,B17,B16,B33,B32,B13,B12,B29,B28,B9,B8,B25,B24,B5,B4,C34,D35,D5,D4,B21,B20,F5,F4,E3,E2,K8,K7,J7,J6,F8,F7,E7,E6,K11,K10,J10,J9,F11,F10,E10,E9,K14,K13,J13,J12,F14,F13,E13,E12,J16,J15,F17,F16,E16,E15,K17,K16,J19,J18,F20,F19,E19,E18,K20,K19,J22,J21,K23,K22,K26,K25,J25,J24,F23,F22,E22,E21,F26,F25,E25,E24,K29,K28,J28,J27,F29,F28,E28,E27,K32,K31,J31,J30,F32,F31,E31,E30,K35,K34,J34,J33,F35,F34,K38,K37,J37,J36,E34,E33,F38,F37,E37,E36,G7,G6,D9,D8,H8,H7,G10,G9,H11,H10,D12,D11,C11,C10,H14,H13,G13,G12,D15,D14,C15,C14,H17,H16,G16,G15,D18,D17,C19,C18,H20,H19,G19,G18,D21,D20,C23,C22,H23,H22,G22,G21,H26,H25,G25,G24,D24,D23,H29,H28,G28,G27,D27,D26,C27,C26,H32,H31,G31,G30,H35,H34,G34,G33,H38,H37,G37,G36,D1,F1,H2,B40,B1,C30,C31,D29,D30,D31,D33,D34);
endinterface
interface ifmchpcpin();
wire G3,G2,H5,H4,J3,J2,K5,K4,C3,C2,C7,C6,A23,A22,A3,A2,A27,A26,A7,A6,A31,A30,A11,A10,A35,A34,A15,A14,A39,A38,A19,A18,B37,B36,B17,B16,B33,B32,B13,B12,B29,B28,B9,B8,B25,B24,B5,B4,C34,D35,D5,D4,B21,B20,F5,F4,E3,E2,K8,K7,J7,J6,F8,F7,E7,E6,K11,K10,J10,J9,F11,F10,E10,E9,K14,K13,J13,J12,F14,F13,E13,E12,J16,J15,F17,F16,E16,E15,K17,K16,J19,J18,F20,F19,E19,E18,K20,K19,J22,J21,K23,K22,K26,K25,J25,J24,F23,F22,E22,E21,F26,F25,E25,E24,K29,K28,J28,J27,F29,F28,E28,E27,K32,K31,J31,J30,F32,F31,E31,E30,K35,K34,J34,J33,F35,F34,K38,K37,J37,J36,E34,E33,F38,F37,E37,E36,G7,G6,D9,D8,H8,H7,G10,G9,H11,H10,D12,D11,C11,C10,H14,H13,G13,G12,D15,D14,C15,C14,H17,H16,G16,G15,D18,D17,C19,C18,H20,H19,G19,G18,D21,D20,C23,C22,H23,H22,G22,G21,H26,H25,G25,G24,D24,D23,H29,H28,G28,G27,D27,D26,C27,C26,H32,H31,G31,G30,H35,H34,G34,G33,H38,H37,G37,G36,D1,F1,H2,B40,B1,C30,C31,D29,D30,D31,D33,D34;
modport pin(inout
G3,G2,H5,H4,J3,J2,K5,K4,C3,C2,C7,C6,A23,A22,A3,A2,A27,A26,A7,A6,A31,A30,A11,A10,A35,A34,A15,A14,A39,A38,A19,A18,B37,B36,B17,B16,B33,B32,B13,B12,B29,B28,B9,B8,B25,B24,B5,B4,C34,D35,D5,D4,B21,B20,F5,F4,E3,E2,K8,K7,J7,J6,F8,F7,E7,E6,K11,K10,J10,J9,F11,F10,E10,E9,K14,K13,J13,J12,F14,F13,E13,E12,J16,J15,F17,F16,E16,E15,K17,K16,J19,J18,F20,F19,E19,E18,K20,K19,J22,J21,K23,K22,K26,K25,J25,J24,F23,F22,E22,E21,F26,F25,E25,E24,K29,K28,J28,J27,F29,F28,E28,E27,K32,K31,J31,J30,F32,F31,E31,E30,K35,K34,J34,J33,F35,F34,K38,K37,J37,J36,E34,E33,F38,F37,E37,E36,G7,G6,D9,D8,H8,H7,G10,G9,H11,H10,D12,D11,C11,C10,H14,H13,G13,G12,D15,D14,C15,C14,H17,H16,G16,G15,D18,D17,C19,C18,H20,H19,G19,G18,D21,D20,C23,C22,H23,H22,G22,G21,H26,H25,G25,G24,D24,D23,H29,H28,G28,G27,D27,D26,C27,C26,H32,H31,G31,G30,H35,H34,G34,G33,H38,H37,G37,G36,D1,F1,H2,B40,B1,C30,C31,D29,D30,D31,D33,D34);
endinterface
module fmchpcsigpin(ifmchpcsig sig,ifmchpcpin pin);
viapairs #(.WIDTH(226))
viabus (sig.CLK0_C2M_N,pin.G3,sig.CLK0_C2M_P,pin.G2,sig.CLK0_M2C_N,pin.H5,sig.CLK0_M2C_P,pin.H4,sig.CLK1_C2M_N,pin.J3,sig.CLK1_C2M_P,pin.J2,sig.CLK1_M2C_N,pin.K5,sig.CLK1_M2C_P,pin.K4,sig.DP0_C2M_N,pin.C3,sig.DP0_C2M_P,pin.C2,sig.DP0_M2C_N,pin.C7,sig.DP0_M2C_P,pin.C6,sig.DP1_C2M_N,pin.A23,sig.DP1_C2M_P,pin.A22,sig.DP1_M2C_N,pin.A3,sig.DP1_M2C_P,pin.A2,sig.DP2_C2M_N,pin.A27,sig.DP2_C2M_P,pin.A26,sig.DP2_M2C_N,pin.A7,sig.DP2_M2C_P,pin.A6,sig.DP3_C2M_N,pin.A31,sig.DP3_C2M_P,pin.A30,sig.DP3_M2C_N,pin.A11,sig.DP3_M2C_P,pin.A10,sig.DP4_C2M_N,pin.A35,sig.DP4_C2M_P,pin.A34,sig.DP4_M2C_N,pin.A15,sig.DP4_M2C_P,pin.A14,sig.DP5_C2M_N,pin.A39,sig.DP5_C2M_P,pin.A38,sig.DP5_M2C_N,pin.A19,sig.DP5_M2C_P,pin.A18,sig.DP6_C2M_N,pin.B37,sig.DP6_C2M_P,pin.B36,sig.DP6_M2C_N,pin.B17,sig.DP6_M2C_P,pin.B16,sig.DP7_C2M_N,pin.B33,sig.DP7_C2M_P,pin.B32,sig.DP7_M2C_N,pin.B13,sig.DP7_M2C_P,pin.B12,sig.DP8_C2M_N,pin.B29,sig.DP8_C2M_P,pin.B28,sig.DP8_M2C_N,pin.B9,sig.DP8_M2C_P,pin.B8,sig.DP9_C2M_N,pin.B25,sig.DP9_C2M_P,pin.B24,sig.DP9_M2C_N,pin.B5,sig.DP9_M2C_P,pin.B4,sig.GA0,pin.C34,sig.GA1,pin.D35,sig.GBTCLK0_M2C_N,pin.D5,sig.GBTCLK0_M2C_P,pin.D4,sig.GBTCLK1_M2C_N,pin.B21,sig.GBTCLK1_M2C_P,pin.B20,sig.HA00_N_CC,pin.F5,sig.HA00_P_CC,pin.F4,sig.HA01_N_CC,pin.E3,sig.HA01_P_CC,pin.E2,sig.HA02_N,pin.K8,sig.HA02_P,pin.K7,sig.HA03_N,pin.J7,sig.HA03_P,pin.J6,sig.HA04_N,pin.F8,sig.HA04_P,pin.F7,sig.HA05_N,pin.E7,sig.HA05_P,pin.E6,sig.HA06_N,pin.K11,sig.HA06_P,pin.K10,sig.HA07_N,pin.J10,sig.HA07_P,pin.J9,sig.HA08_N,pin.F11,sig.HA08_P,pin.F10,sig.HA09_N,pin.E10,sig.HA09_P,pin.E9,sig.HA10_N,pin.K14,sig.HA10_P,pin.K13,sig.HA11_N,pin.J13,sig.HA11_P,pin.J12,sig.HA12_N,pin.F14,sig.HA12_P,pin.F13,sig.HA13_N,pin.E13,sig.HA13_P,pin.E12,sig.HA14_N,pin.J16,sig.HA14_P,pin.J15,sig.HA15_N,pin.F17,sig.HA15_P,pin.F16,sig.HA16_N,pin.E16,sig.HA16_P,pin.E15,sig.HA17_N_CC,pin.K17,sig.HA17_P_CC,pin.K16,sig.HA18_N,pin.J19,sig.HA18_P,pin.J18,sig.HA19_N,pin.F20,sig.HA19_P,pin.F19,sig.HA20_N,pin.E19,sig.HA20_P,pin.E18,sig.HA21_N,pin.K20,sig.HA21_P,pin.K19,sig.HA22_N,pin.J22,sig.HA22_P,pin.J21,sig.HA23_N,pin.K23,sig.HA23_P,pin.K22,sig.HB00_N_CC,pin.K26,sig.HB00_P_CC,pin.K25,sig.HB01_N,pin.J25,sig.HB01_P,pin.J24,sig.HB02_N,pin.F23,sig.HB02_P,pin.F22,sig.HB03_N,pin.E22,sig.HB03_P,pin.E21,sig.HB04_N,pin.F26,sig.HB04_P,pin.F25,sig.HB05_N,pin.E25,sig.HB05_P,pin.E24,sig.HB06_N_CC,pin.K29,sig.HB06_P_CC,pin.K28,sig.HB07_N,pin.J28,sig.HB07_P,pin.J27,sig.HB08_N,pin.F29,sig.HB08_P,pin.F28,sig.HB09_N,pin.E28,sig.HB09_P,pin.E27,sig.HB10_N,pin.K32,sig.HB10_P,pin.K31,sig.HB11_N,pin.J31,sig.HB11_P,pin.J30,sig.HB12_N,pin.F32,sig.HB12_P,pin.F31,sig.HB13_N,pin.E31,sig.HB13_P,pin.E30,sig.HB14_N,pin.K35,sig.HB14_P,pin.K34,sig.HB15_N,pin.J34,sig.HB15_P,pin.J33,sig.HB16_N,pin.F35,sig.HB16_P,pin.F34,sig.HB17_N_CC,pin.K38,sig.HB17_P_CC,pin.K37,sig.HB18_N,pin.J37,sig.HB18_P,pin.J36,sig.HB19_N,pin.E34,sig.HB19_P,pin.E33,sig.HB20_N,pin.F38,sig.HB20_P,pin.F37,sig.HB21_N,pin.E37,sig.HB21_P,pin.E36,sig.LA00_N_CC,pin.G7,sig.LA00_P_CC,pin.G6,sig.LA01_N_CC,pin.D9,sig.LA01_P_CC,pin.D8,sig.LA02_N,pin.H8,sig.LA02_P,pin.H7,sig.LA03_N,pin.G10,sig.LA03_P,pin.G9,sig.LA04_N,pin.H11,sig.LA04_P,pin.H10,sig.LA05_N,pin.D12,sig.LA05_P,pin.D11,sig.LA06_N,pin.C11,sig.LA06_P,pin.C10,sig.LA07_N,pin.H14,sig.LA07_P,pin.H13,sig.LA08_N,pin.G13,sig.LA08_P,pin.G12,sig.LA09_N,pin.D15,sig.LA09_P,pin.D14,sig.LA10_N,pin.C15,sig.LA10_P,pin.C14,sig.LA11_N,pin.H17,sig.LA11_P,pin.H16,sig.LA12_N,pin.G16,sig.LA12_P,pin.G15,sig.LA13_N,pin.D18,sig.LA13_P,pin.D17,sig.LA14_N,pin.C19,sig.LA14_P,pin.C18,sig.LA15_N,pin.H20,sig.LA15_P,pin.H19,sig.LA16_N,pin.G19,sig.LA16_P,pin.G18,sig.LA17_N_CC,pin.D21,sig.LA17_P_CC,pin.D20,sig.LA18_N_CC,pin.C23,sig.LA18_P_CC,pin.C22,sig.LA19_N,pin.H23,sig.LA19_P,pin.H22,sig.LA20_N,pin.G22,sig.LA20_P,pin.G21,sig.LA21_N,pin.H26,sig.LA21_P,pin.H25,sig.LA22_N,pin.G25,sig.LA22_P,pin.G24,sig.LA23_N,pin.D24,sig.LA23_P,pin.D23,sig.LA24_N,pin.H29,sig.LA24_P,pin.H28,sig.LA25_N,pin.G28,sig.LA25_P,pin.G27,sig.LA26_N,pin.D27,sig.LA26_P,pin.D26,sig.LA27_N,pin.C27,sig.LA27_P,pin.C26,sig.LA28_N,pin.H32,sig.LA28_P,pin.H31,sig.LA29_N,pin.G31,sig.LA29_P,pin.G30,sig.LA30_N,pin.H35,sig.LA30_P,pin.H34,sig.LA31_N,pin.G34,sig.LA31_P,pin.G33,sig.LA32_N,pin.H38,sig.LA32_P,pin.H37,sig.LA33_N,pin.G37,sig.LA33_P,pin.G36,sig.PG_C2M,pin.D1,sig.PG_M2C,pin.F1,sig.PRSNT_M2C_L,pin.H2,sig.RES0,pin.B40,sig.RES1,pin.B1,sig.SCL,pin.C30,sig.SDA,pin.C31,sig.TCK,pin.D29,sig.TDI,pin.D30,sig.TDO,pin.D31,sig.TMS,pin.D33,sig.TRST_L,pin.D34
);
endmodule
interface fmc120signame(); // fmc120 sig
wire LLMK_DCLKOUT_2_P,LLMK_DCLKOUT_2_N,LLMK_SCLKOUT_3_P,LLMK_SCLKOUT_3_N,LMK_DCLK11_M2C_TO_FPGA_P,LMK_DCLK11_M2C_TO_FPGA_N,GBTCLK1_CXR_M2C_P,GBTCLK1_CXR_M2C_N,DP0_C2M_P,DP0_C2M_N,DP1_C2M_P,DP1_C2M_N,DP2_C2M_P,DP2_C2M_N,DP3_C2M_P,DP3_C2M_N,DP4_C2M_P,DP4_C2M_N,DP5_C2M_P,DP5_C2M_N,DP6_C2M_P,DP6_C2M_N,DP7_C2M_P,DP7_C2M_N,DP0_M2C_P,DP0_M2C_N,DP1_M2C_P,DP1_M2C_N,DP2_M2C_P,DP2_M2C_N,DP3_M2C_P,DP3_M2C_N,DP4_M2C_P,DP4_M2C_N,DP5_M2C_P,DP5_M2C_N,DP6_M2C_P,DP6_M2C_N,DP7_M2C_P,DP7_M2C_N,BUF_EXT_TRIG_TO_FPGA_P,BUF_EXT_TRIG_TO_FPGA_N,FPGA_SYNC_OUT_P,FPGA_SYNC_OUT_N,DAC_SYNC_REQ_TO_FPGA_P,DAC_SYNC_REQ_TO_FPGA_N,FMC_CPLD_CTRL0_VADJ,FMC_CPLD_CTRL1_VADJ,FMC_CPLD_CTRL2_VADJ,FMC_CPLD_CTRL3_VADJ,FPGA_TRIGGER_OUT_VADJ,FPGA_SYNC_OUT_TO_LMK_VADJ,ADCB_SYNC_IN_L_VADJ,ADCA_SYNC_IN_L_VADJ,ADCB_POWER_DOWN_A_OVR_VADJ,ADCB_SDOUT_B_OVR_VADJ,ADCA_POWER_DOWN_A_OVR_VADJ,ADCA_SDOUT_B_OVR_VADJ,DAC_TXEN_VADJ,GA0,GA1,TRST_L,TCK,TMS,TDI,TDO,PRSNT_M2C_CC_L,PG_M2C,PG_C2M,I2C_SCL,I2C_SDA,RES0,CLK_DIR;
endinterface
interface ivc707();
wire si5324_int_alm;
wire si5324_rst;
wire si5324_out_c;
wire sysclk;
wire rec_clock;
wire sma_mgt_refclk;
wire sma_mgt_tx_p;
wire sma_mgt_rx_p;
wire sma_mgt_tx_n;
wire sma_mgt_rx_n;
wire fmc1_hpc_gbtclk1_m2c_c_p;
wire fmc1_hpc_gbtclk1_m2c_c_n;
wire fmc1_hpc_gbtclk0_m2c_c_p;
wire fmc1_hpc_gbtclk0_m2c_c_n;
wire fmc2_hpc_gbtclk1_m2c_c_p;
wire fmc2_hpc_gbtclk1_m2c_c_n;
wire fmc2_hpc_gbtclk0_m2c_c_p;
wire fmc2_hpc_gbtclk0_m2c_c_n;
wire rotary_push;
wire rotary_inca;
wire rotary_incb;
wire gpio_led_0;
wire gpio_led_1;
wire gpio_led_2;
wire gpio_led_3;
wire gpio_led_4;
wire gpio_led_5;
wire gpio_led_6;
wire gpio_led_7;
wire user_sma_gpio_p;
wire user_sma_gpio_n;
wire user_sma_clock_p;
wire user_sma_clock_n;
wire user_clock_p;
wire user_clock_n;
wire gpio_dip_sw0;
wire gpio_dip_sw1;
wire gpio_dip_sw2;
wire gpio_dip_sw3;
wire gpio_dip_sw4;
wire gpio_dip_sw5;
wire gpio_dip_sw6;
wire gpio_dip_sw7;
wire cpu_reset;
wire gpio_sw_s;
wire gpio_sw_n;
wire gpio_sw_c;
wire gpio_sw_e;
wire gpio_sw_w;
wire phy_reset;
wire phy_mdio;
wire phy_int;
wire phy_mdc;
wire sgmiiclk;
wire sgmii_tx_p;
wire sgmii_rx_p;
wire sgmii_tx_n;
wire sgmii_rx_n;
wire fmc_vadj_on_b_ls;
wire pmbus_alert;
wire pmbus_data;
wire pmbus_clk;
wire VP_0;
wire VN_0;
ifmchpcpin fmc2pin();
ifmchpcpin fmc1pin();
//fmc120sig fmc1sig();
//fmc120sig fmc2sig();
ddr3 ddr3();
flash flash();
hdmi hdmi();
iic iic();
lcd lcd();
ipciex8 pcie();
sdio sdio();
sfp sfp();
usbsmsc usbsmsc();
usb2uart usb2uart();
xadc xadc();
wire sm_fan_pwm;
wire sm_fan_tach;
modport hw(inout fmc_vadj_on_b_ls,phy_mdio,pmbus_alert,pmbus_clk,pmbus_data,user_clock_n,user_clock_p,user_sma_clock_n,user_sma_clock_p,user_sma_gpio_n,user_sma_gpio_p,sma_mgt_tx_n,sma_mgt_tx_p
,input gpio_led_0,gpio_led_1,gpio_led_2,gpio_led_3,gpio_led_4,gpio_led_5,gpio_led_6,gpio_led_7,sgmii_tx_n,sgmii_tx_p,si5324_rst,sm_fan_pwm,phy_reset,phy_mdc
,output cpu_reset,fmc1_hpc_gbtclk0_m2c_c_n,fmc1_hpc_gbtclk0_m2c_c_p,fmc1_hpc_gbtclk1_m2c_c_n,fmc1_hpc_gbtclk1_m2c_c_p,fmc2_hpc_gbtclk0_m2c_c_n,fmc2_hpc_gbtclk0_m2c_c_p,fmc2_hpc_gbtclk1_m2c_c_n,fmc2_hpc_gbtclk1_m2c_c_p,gpio_dip_sw0,gpio_dip_sw1,gpio_dip_sw2,gpio_dip_sw3,gpio_dip_sw4,gpio_dip_sw5,gpio_dip_sw6,gpio_dip_sw7,gpio_sw_c,gpio_sw_e,gpio_sw_n,gpio_sw_s,gpio_sw_w,rec_clock,rotary_inca,rotary_incb,rotary_push,sgmii_rx_n,sgmii_rx_p,sgmiiclk,si5324_int_alm,si5324_out_c,sm_fan_tach,sma_mgt_refclk,sma_mgt_rx_n,sma_mgt_rx_p,sysclk,VP_0,VN_0,phy_int
);
modport cfg(inout phy_mdio,user_clock_n,user_clock_p,user_sma_clock_n,user_sma_clock_p,user_sma_gpio_n,user_sma_gpio_p,sma_mgt_tx_n,sma_mgt_tx_p
,input cpu_reset,gpio_dip_sw0,gpio_dip_sw1,gpio_dip_sw2,gpio_dip_sw3,gpio_dip_sw4,gpio_dip_sw5,gpio_dip_sw6,gpio_dip_sw7,gpio_sw_c,gpio_sw_e,gpio_sw_n,gpio_sw_s,gpio_sw_w,rotary_inca,rotary_incb,rotary_push,sgmii_rx_n,sgmii_rx_p,sgmiiclk,sysclk,VP_0,VN_0,sma_mgt_rx_n,sma_mgt_rx_p,si5324_int_alm,sma_mgt_refclk,si5324_out_c,phy_int
,output gpio_led_0,gpio_led_1,gpio_led_2,gpio_led_3,gpio_led_4,gpio_led_5,gpio_led_6,gpio_led_7,phy_mdc,phy_reset,sgmii_tx_n,sgmii_tx_p
);
modport sim(
inout fmc_vadj_on_b_ls,phy_mdio,pmbus_alert,pmbus_clk,pmbus_data,user_clock_n,user_clock_p,user_sma_clock_n,user_sma_clock_p,user_sma_gpio_n,user_sma_gpio_p,sma_mgt_tx_n,sma_mgt_tx_p
,output gpio_led_0,gpio_led_1,gpio_led_2,gpio_led_3,gpio_led_4,gpio_led_5,gpio_led_6,gpio_led_7,phy_mdc,phy_reset,sgmii_tx_n,sgmii_tx_p,si5324_rst,sm_fan_pwm,sma_mgt_rx_n,sma_mgt_rx_p
,input cpu_reset,fmc1_hpc_gbtclk0_m2c_c_n,fmc1_hpc_gbtclk0_m2c_c_p,fmc1_hpc_gbtclk1_m2c_c_n,fmc1_hpc_gbtclk1_m2c_c_p,fmc2_hpc_gbtclk0_m2c_c_n,fmc2_hpc_gbtclk0_m2c_c_p,fmc2_hpc_gbtclk1_m2c_c_n,fmc2_hpc_gbtclk1_m2c_c_p,gpio_dip_sw0,gpio_dip_sw1,gpio_dip_sw2,gpio_dip_sw3,gpio_dip_sw4,gpio_dip_sw5,gpio_dip_sw6,gpio_dip_sw7,gpio_sw_c,gpio_sw_e,gpio_sw_n,gpio_sw_s,gpio_sw_w,rec_clock,rotary_inca,rotary_incb,rotary_push,sgmii_rx_n,sgmii_rx_p,sgmiiclk,si5324_int_alm,si5324_out_c,sm_fan_tach,sma_mgt_refclk,sysclk,VP_0,VN_0,phy_int
);

endinterface
