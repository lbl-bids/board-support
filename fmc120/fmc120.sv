`timescale 1ns / 100ps
module fmc120(ifmchpcpin.pin fmcpin,ifmc120.hw fmc120);
fmc120_v fmc120_v(
`include "fmcpin_vinst.vh"
,.fmc120(fmc120)
);
endmodule
module fmc120_v(
`include "fmcpin_vport.vh"
,ifmc120.hw fmc120
);
ifmchpcpin fmcpin();
`include "fmcpin_via.vh"
fmc120_i fmc120_i(.fmcpin(fmcpin.pin),.fmc120(fmc120));
endmodule

module fmc120_i(ifmchpcpin.pin fmcpin,ifmc120.hw fmc120);
`include "fmc120.vh"
endmodule

interface ifmc120 ();
`include "ifmc120.vh"
endinterface
module fmc120_2(ifmchpcpin_2.pin fmcpin,ifmc120_2.hw fmc120);

`include "fmc120.vh"
endmodule

interface ifmc120_2 ();
`include "ifmc120.vh"
endinterface
