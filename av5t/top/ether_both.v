`timescale 1ns / 1ns

// Version for Virtex-5 MGT

module ether_both(
	// Stupid non-general-purpose FPGA pins
	input   refclk_p,
	input   refclk_n,
	input   rxn0,
	input   rxp0,
	output  txn0,
	output  txp0,
	input   rxn1,
	input   rxp1,
	output  txn1,
	output  txp1,

	// Faceplate GMII port
	input GMII_MCLK,
	input GMII_RX_CLK,
	input [7:0] GMII_RXD,
	input GMII_RX_DV,
	input GMII_RX_ER,  // not used XXX that's a mistake
	output GMII_GTX_CLK,
	input  GMII_TX_CLK,  // not used
	output reg [7:0] GMII_TXD,
	output reg GMII_TX_EN,
	output reg GMII_TX_ER,

	// Faceplate Ethernet pins
	output GBE_FP_MDC,
	inout  GBE_FP_MDIO,
	output GBE_FP_RSTN,

	// Auxiliary Ethernet pins
	output GBE_AX_MDC,
	inout  GBE_AX_MDIO,
	output GBE_AX_RSTN,

	// SFP 0 pins
	input  SFP0_LOS,
	input  SFP0_MOD0,
	output SFP0_MOD1,
	inout  SFP0_MOD2,
	output SFP0_TX_DISABLE,

	// SFP 1 pins
	input  SFP1_LOS,
	input  SFP1_MOD0,
	output SFP1_MOD1,
	inout  SFP1_MOD2,
	output SFP1_TX_DISABLE,

	output [7:0] LED,
	output [5:0] DEBUGO
);
// LED assignment derived from bit assignments here,
// in aggregate.v, and in client_rx.v.
//   LED[0] D7: Rx CRC OK (sticks on after a single packet passes CRC test)
//   LED[1] D8: 3.7 Hz clock-present blinker
//   LED[2] D1: brightness controlled by UDP port 1000, payload byte 1
//   LED[3] D2: brightness controlled by UDP port 1000, payload byte 2
//   LED[4] D3: ARP packet rejected
//   LED[5] D4: ARP packet accepted
//   LED[6] D5: Rx packet activity
//   LED[7] D6: Tx packet activity

parameter [31:0] ip = 32'd3232237570;  // 192.168.8.2
parameter [47:0] mac = 48'h00105ad152b4;  // scraped from a 3C905B

//rameter [31:0] ip2 = 32'd3232237590;  // 192.168.8.22
//rameter [31:0] ip2 = 32'd2213805613;  // 131.243.254.45 Bldg. 50 test
parameter [31:0] ip2 = 32'h800380ac;    // 128.3.128.172 (lrd3) Bldg. 71
parameter [47:0] mac2 = 48'h00105ad155b3;  // fictitious

reg [3:0] power_conf_iob=4'b0000;// Start all off (except for FP Eth, see below)

// ============= Ethernet on SFP0 follows ===================

// The two clocks are sourced from gmii_link
wire rx_clk, tx_clk;

// Stupid resets
reg gtp_reset=1, gtp_reset1=1;
always @(posedge tx_clk) begin
	gtp_reset <= gtp_reset1;
	gtp_reset1 <= 0;
end

// Virtex-5 MGT wrapper on top of wrapper on ...
wire [9:0] txdata0, txdata1, rxdata0, rxdata1;
wire [6:0] rxstatus0, rxstatus1;  // XXX not hooked up?
wire txstatus0, txstatus1;
gtp_wrap2 gtp_wrap_i(
	.txdata0(txdata0), .txstatus0(txstatus0),
	.rxdata0(rxdata0), .rxstatus0(rxstatus0),
	.txdata1(txdata1), .txstatus1(txstatus1),
	.rxdata1(rxdata1), .rxstatus1(rxstatus1),
	.tx_clk(tx_clk), .rx_clk(rx_clk), .gtp_reset(gtp_reset),
	.refclk_p(refclk_p), .refclk_n(refclk_n),
	.rxn0(rxn0), .rxp0(rxp0),
	.txn0(txn0), .txp0(txp0),
	.rxn1(rxn1), .rxp1(rxp1),
	.txn1(txn1), .txp1(txp1)
);

wire [2:0] debug;

// bridge between serdes and internal GMII
// watch the clock domains!
wire [7:0] abst2_in, abst2_out, rxd;
wire abst2_in_s, abst2_out_s, rx_dv;
reg rd=0;  // Running Disparity, see below
reg rd_hack=0, rd_hack0=0;  // Software-writable
always @(posedge tx_clk) rd_hack <= rd_hack0;
wire [5:0] link_leds;
wire [15:0] lacr_rx;  // nominally in Rx clock domain, don't sweat it
wire [1:0] an_state_mon;
reg an_bypass=1;  // settable by software
gmii_link glink(
	.RX_CLK(rx_clk),
	.RXD(rxd),
	.RX_DV(rx_dv),
	.GTX_CLK(tx_clk),
	.TXD(abst2_out),
	.TX_EN(abst2_out_s),
	.TX_ER(1'b0),
	.txdata(txdata0),
	.rxdata(rxdata0),
	.rx_err_los(rxstatus0[4]),
	.an_bypass(an_bypass),
	.lacr_rx(lacr_rx),
	.an_state_mon(an_state_mon),
	.leds(link_leds)
);

// Trace logic on the data source side, rx_clk domain
`ifdef TRACE_RX
wire [31:0] trace_in = {4'h4,rx_dv,1'h1,rxd,rxstatus0,rxdata0};
wire trace_clk=rx_clk;
`else
wire [31:0] trace_in = {4'h4,abst2_out_s,3'h1,abst2_out,4'b0,txstatus0,txdata0};
wire trace_clk=tx_clk;
`endif

// FIFO from Rx clock domain to Tx clock domain
gmii_fifo rx2tx(
	.clk_in(rx_clk), .d_in(rxd),   .strobe_in(rx_dv),
	.clk_out(tx_clk),   .d_out(abst2_in), .strobe_out(abst2_in_s)
);

// Single clock domain, abstract Ethernet
wire rx_crc_ok2;
wire [7:0] data_rx2_1;  wire ready2_1, strobe_rx2_1, crc_rx2_1;
wire [7:0] data_rx2_2;  wire ready2_2, strobe_rx2_2, crc_rx2_2;
wire [7:0] data_rx2_3;  wire ready2_3, strobe_rx2_3, crc_rx2_3;
wire [7:0] data_tx2_1;  wire [10:0] length2_1;  wire req2_1, ack2_1, warn2_1, strobe_tx2_1;
wire [7:0] data_tx2_2;  wire [10:0] length2_2;  wire req2_2, ack2_2, warn2_2, strobe_tx2_2;
wire [7:0] data_tx2_3;  wire [10:0] length2_3;  wire req2_3, ack2_3, warn2_3, strobe_tx2_3;
wire [3:0] abst2_leds;
aggregate #(.ip(ip2), .mac(mac2)) a2(.clk(tx_clk),
	.eth_in(abst2_in),   .eth_in_s(abst2_in_s),
	.eth_out(abst2_out), .eth_out_s(abst2_out_s),
	.rx_crc_ok(rx_crc_ok2),
	.address_set(9'b0),

	.data_rx_1(data_rx2_1), .ready_1(ready2_1), .strobe_rx_1(strobe_rx2_1), .crc_rx_1(crc_rx2_1),
	.data_rx_2(data_rx2_2), .ready_2(ready2_2), .strobe_rx_2(strobe_rx2_2), .crc_rx_2(crc_rx2_2),
	.data_rx_3(data_rx2_3), .ready_3(ready2_3), .strobe_rx_3(strobe_rx2_3), .crc_rx_3(crc_rx2_3),

	.req_1(req2_1), .length_1(length2_1), .ack_1(ack2_1), .warn_1(warn2_1), .strobe_tx_1(strobe_tx2_1), .data_tx_1(data_tx2_1),
	.req_2(req2_2), .length_2(length2_2), .ack_2(ack2_2), .warn_2(warn2_2), .strobe_tx_2(strobe_tx2_2), .data_tx_2(data_tx2_2),
	.req_3(req2_3), .length_3(length2_3), .ack_3(ack2_3), .warn_3(warn2_3), .strobe_tx_3(strobe_tx2_3), .data_tx_3(data_tx2_3),

	.debug(debug), .leds(abst2_leds));

wire [23:0] control2_addr;
wire control2_strobe, control2_rd;
wire [31:0] data2_out;
reg [31:0] data2_in=0;
mem_gateway sfp_cl3(.clk(tx_clk), .rx_ready(ready2_3), .rx_strobe(strobe_rx2_3),
	.rx_crc(crc_rx2_3), .packet_in(data_rx2_3),
	.tx_ack(ack2_3), .tx_strobe(warn2_3), .tx_req(req2_3), .tx_len(length2_3), .packet_out(data_tx2_3),
	.addr(control2_addr), .control_strobe(control2_strobe), .control_rd(control2_rd),
	.data_out(data2_out), .data_in(data2_in));

// Stupid test rig, clock domain matches aggregate
always @(posedge tx_clk) case (control2_addr[2:0])
	0: data2_in <= "Hell";
	1: data2_in <= "o wo";
	2: data2_in <= "rld!";
	3: data2_in <= 32'h0d0a0d0a;
endcase

// trace logic, now that the source is defined
reg [10:0] trace_wadd=0;
reg [31:0] trace_in1=0, trace_in2=0;  // input history
// actually save trace_in2 so we get old as well as triggering data
wire trace_trig = trace_in2[8:0] != trace_in[8:0];  // sensitive to changes in alternating rx_data0
reg trace_req1=0;  // forward reference
reg trace_nowait=0;  // software controlled
reg trace_run=0, trace_req2=0, trace_done=0;
wire trace_ending=trace_wadd==11'h7ff;
always @(posedge trace_clk) begin
	trace_wadd <= trace_run ? (trace_wadd+1) : 0;
	trace_in1 <= trace_in;
	trace_in2 <= trace_in1;
	trace_req2 <= trace_req1;  // cross clock domains
	if (trace_req2 & ~trace_run & (trace_nowait | trace_trig)) trace_run <= 1;
	if (trace_ending) trace_run <= 0;
	if (trace_ending) trace_done <= 1;  // flag to requesting clock domain
	if (~trace_req2) trace_done <= 0;
end
// dpram instantiation later

// ============= Faceplate GMII Ethernet follows ===================

assign GMII_GTX_CLK=GMII_MCLK;
wire fp_clk=GMII_MCLK;   // at some point we want a ring clock instead

// Latch Rx input pins in IOB
reg [7:0] rx1d=0;
reg rx1_dv=0, rx1_er=0;
always @(posedge GMII_RX_CLK) begin
	rx1d   <= GMII_RXD;
	rx1_dv <= GMII_RX_DV;
	rx1_er <= GMII_RX_ER;
end
// FIFO from Rx clock domain to ring clock domain
wire [7:0] abst1_in, abst1_out;
wire abst1_in_s, abst1_out_s;
gmii_fifo rx2ring(
	.clk_in(GMII_RX_CLK), .d_in(rx1d),  .strobe_in(rx1_dv),
	.clk_out(fp_clk),    .d_out(abst1_in), .strobe_out(abst1_in_s)
);

// Single clock domain, abstract Ethernet
wire rx_crc_ok1;
wire [7:0] data_rx_1;  wire ready_1, strobe_rx_1, crc_rx_1;
wire [7:0] data_rx_2;  wire ready_2, strobe_rx_2, crc_rx_2;
wire [7:0] data_rx_3;  wire ready_3, strobe_rx_3, crc_rx_3;
wire [7:0] data_tx_1;  wire [10:0] length_1;  wire req_1, ack_1, warn_1, strobe_tx_1;
wire [7:0] data_tx_2;  wire [10:0] length_2;  wire req_2, ack_2, warn_2, strobe_tx_2;
wire [7:0] data_tx_3;  wire [10:0] length_3;  wire req_3, ack_3, warn_3, strobe_tx_3;
wire [3:0] abst1_leds;
aggregate #(.ip(ip), .mac(mac)) a1(.clk(fp_clk),
	.eth_in(abst1_in),   .eth_in_s(abst1_in_s),
	.eth_out(abst1_out), .eth_out_s(abst1_out_s),
	.rx_crc_ok(rx_crc_ok1),
	.address_set(9'b0),

	.data_rx_1(data_rx_1), .ready_1(ready_1), .strobe_rx_1(strobe_rx_1), .crc_rx_1(crc_rx_1),
	.data_rx_2(data_rx_2), .ready_2(ready_2), .strobe_rx_2(strobe_rx_2), .crc_rx_2(crc_rx_2),
	.data_rx_3(data_rx_3), .ready_3(ready_3), .strobe_rx_3(strobe_rx_3), .crc_rx_3(crc_rx_3),

	.req_1(req_1), .length_1(length_1), .ack_1(ack_1), .warn_1(warn_1), .strobe_tx_1(strobe_tx_1), .data_tx_1(data_tx_1),
	.req_2(req_2), .length_2(length_2), .ack_2(ack_2), .warn_2(warn_2), .strobe_tx_2(strobe_tx_2), .data_tx_2(data_tx_2),
	.req_3(req_3), .length_3(length_3), .ack_3(ack_3), .warn_3(warn_3), .strobe_tx_3(strobe_tx_3), .data_tx_3(data_tx_3),

	.leds(abst1_leds));

// instantiate some test clients
// Tx only, but triggered by corresponding Rx ready
reg [7:0] client_txu_config=8'h13;
wire [3:0] sfp_static={SFP1_LOS, SFP1_MOD0, SFP0_LOS, SFP0_MOD0};
client_txu mut(.clk(fp_clk), .ack(ack_1), .strobe(warn_1),
	.req(req_1), .length(length_1), .data_out(data_tx_1),
	.rx_clk(rx_clk), .tx_clk(tx_clk), .gr_clk(GMII_RX_CLK),
	.if_config(client_txu_config),
	.GBE_FP_MDC(GBE_FP_MDC), .GBE_FP_MDIO(GBE_FP_MDIO),
	.GBE_AX_MDC(GBE_AX_MDC), .GBE_AX_MDIO(GBE_AX_MDIO),
	.SFP0_MOD1(SFP0_MOD1), .SFP0_MOD2(SFP0_MOD2),
	.SFP1_MOD1(SFP1_MOD1), .SFP1_MOD2(SFP1_MOD2),
	.other({lacr_rx,8'hbe, 2'b11, an_state_mon, sfp_static}));

wire [1:0] led1;
client_rx cl1rx(.clk(fp_clk), .ready(ready_1), .strobe(strobe_rx_1), .crc(crc_rx_1), .data_in(data_rx_1), .led(led1));

reg nomangle=0;
client_thru cl2rxtx(.clk(fp_clk), .rx_ready(ready_2), .rx_strobe(strobe_rx_2), .rx_crc(crc_rx_2), .data_in(data_rx_2),
	.nomangle(nomangle),
	.tx_ack(ack_2), .tx_warn(warn_2), .tx_req(req_2), .tx_len(length_2), .data_out(data_tx_2));

wire [23:0] control1_addr;
wire control1_strobe, control1_rd;
wire [31:0] data1_out;
reg [31:0] data1_in=0;
mem_gateway cl3rxtx(.clk(fp_clk),
	.rx_ready(ready_3), .rx_strobe(strobe_rx_3), .rx_crc(crc_rx_3), .packet_in(data_rx_3),
	.tx_ack(ack_3), .tx_strobe(warn_3), .tx_req(req_3), .tx_len(length_3), .packet_out(data_tx_3),
	.addr(control1_addr), .control_strobe(control1_strobe), .control_rd(control1_rd),
	.data_out(data1_out), .data_in(data1_in));

// Stupid test rig
wire [31:0] trace_ro;
dpram #(.aw(11), .dw(32)) trace(
	.clka(rx_clk), .addra(trace_wadd), .dina(trace_in2), .wena(trace_run),
	.clkb(fp_clk), .addrb(control1_addr[10:0]), .doutb(trace_ro));
always @(posedge fp_clk) case ({control1_addr[12],control1_addr[1:0]})
	0: data1_in <= "Good";
	1: data1_in <= "bye ";
	2: data1_in <= trace_req1 ? "Davi" : "Worl";  // trace has been requested but not yet acknowledged
	3: data1_in <= {"d!", 16'h0d0a};
	4: data1_in <= trace_ro;
	5: data1_in <= trace_ro;
	6: data1_in <= trace_ro;
	7: data1_in <= trace_ro;
endcase
always @(posedge fp_clk) begin
	if (control1_strobe & ~control1_rd & (control1_addr[7:0]==8'h76)) power_conf_iob <= data1_out;
	if (control1_strobe & ~control1_rd & (control1_addr[7:0]==8'h77)) client_txu_config <= data1_out;
end
reg trace_req0=0, trace_done1=0;
always @(posedge fp_clk) begin
	trace_req0 <= (control1_strobe & ~control1_rd & (control1_addr[7:0]==8'h78));
	if (control1_strobe & ~control1_rd & (control1_addr[7:0]==8'h78)) trace_nowait <= data1_out[0];
	trace_done1 <= trace_done;  // cross clock domains
	if (trace_req0) trace_req1 <= 1;  // flag for acknowledging clock domain
	if (trace_done1) trace_req1 <= 0;
end
always @(posedge fp_clk) begin
	if (control1_strobe & ~control1_rd & (control1_addr[7:0]==8'h79))
		{rd_hack0,an_bypass} <= data1_out[1:0];
end
always @(posedge fp_clk) begin
	if (control1_strobe & ~control1_rd & (control1_addr[7:0]==8'h7a)) nomangle <= data1_out[0];
end

// FIFO from ring clock domain to tx clock domain
// No-op in the current configuration
wire [7:0] txd;
wire tx_en;
`ifdef LATER
gmii_fifo ring2tx(
	.clk_in(fp_clk),      .d_in(abst1_out), .strobe_in(abst1_out_s),
	.clk_out(GMII_GTX_CLK), .d_out(txd), .strobe_out(tx_en)
);
`else
assign txd=abst1_out;
assign tx_en=abst1_out_s;
`endif

// Latch Tx output pins in IOB
always @(posedge GMII_GTX_CLK) begin
	GMII_TXD   <= txd;
	GMII_TX_EN <= tx_en;
	GMII_TX_ER <= 0;  // Our logic never needs this
end

// ============= Housekeeping follows ===================

// Simple blinker to show clock exists
reg [24:0] ecnt=0;
always @(posedge rx_clk) ecnt<=ecnt+1;
wire blink=ecnt[24];

// Gigabit Ethernet (GBE) serial ports
assign GBE_FP_RSTN=1'b1;   // power_conf_iob[2];  // take chip out of reset
assign GBE_AX_RSTN=power_conf_iob[3];  // take chip out of reset

// SFP Digital Diagnostics Transceiver Controllers
assign SFP0_TX_DISABLE = ~power_conf_iob[0];
assign SFP1_TX_DISABLE = ~power_conf_iob[1];

assign LED={blink,rx_crc_ok1,link_leds};  // XXX multiplex with dip switch?

assign DEBUGO={2'b0,debug,rx_clk};

endmodule
