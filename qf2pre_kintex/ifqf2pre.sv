interface ifqf2pre(
	);
	wire D4;
	wire D5;
	//	wire U19_p,U19_n;
	//	wire U5_p,U5_n;
	//	wire Y4_p,Y4_n;
	wire U5_clk;
	wire U19_clk;
	wire Y4_clk;
	wire ext_clk;
	wire k7_done_or_rxlocked;
	wire kintex_data_in_p;
	wire kintex_data_in_n;
	wire kintex_data_out_p;
	wire kintex_data_out_n;
	wire sys_clk;
	wire SIT9122_OE;

	wire bmb7_clk,bmb7_clk_4x;
	wire [7:0] port_50006_word_k7tos6;
	wire [7:0] port_50006_word_s6tok7;
	wire port_50006_tx_available,port_50006_tx_complete;
	wire port_50006_rx_available,port_50006_rx_complete;
	wire port_50006_word_read;

	modport hw(input D4,D5,k7_done_or_rxlocked,SIT9122_OE
	,output U5_clk,U19_clk,Y4_clk,ext_clk,sys_clk
	,output kintex_data_in_p,kintex_data_in_n
	,input kintex_data_out_p,kintex_data_out_n
	//U5_p,U5_n,Y4_p,Y4_n,U19_p,U19_n
	);
	modport cfg(output D4,D5,k7_done_or_rxlocked,SIT9122_OE
	,input U5_clk,U19_clk,Y4_clk,ext_clk,sys_clk
	,input kintex_data_in_p,kintex_data_in_n
	,output kintex_data_out_p,kintex_data_out_n
	,output bmb7_clk,bmb7_clk_4x
	,output port_50006_word_s6tok7,port_50006_rx_available,port_50006_rx_complete
	,input port_50006_word_k7tos6,port_50006_tx_available,port_50006_tx_complete,port_50006_word_read

	//U5_p,U5_n,Y4_p,Y4_n,U19_p,U19_n
	);
	modport driver(input port_50006_word_s6tok7,port_50006_rx_available,port_50006_rx_complete
	,input bmb7_clk,bmb7_clk_4x,Y4_clk,U5_clk
	,output port_50006_word_k7tos6,port_50006_tx_available,port_50006_tx_complete,port_50006_word_read
	);
	//modport sim(
	//);
endinterface
