proc qf2v07_io { } {
dict set pinio modulename "qf2v07"
set inlist { fpga.AB16 fpga.AC16 fpga.D5 fpga.D6 fpga.J4 fpga.J3 fpga.L4 fpga.L3 fpga.N4 fpga.N3 fpga.R4 fpga.R3 fpga.B6 fpga.B5 fpga.C4 fpga.C3 fpga.E4 fpga.E3 fpga.G4 fpga.G3 fpga.J13 fpga.H13 }
set outlist { fpga.AA15 fpga.AD14 fpga.Y6 fpga.AA14 fpga.AA2 fpga.AA3 fpga.AB1 fpga.AB20 fpga.AB2 fpga.AB6 fpga.AC14 fpga.AC1 fpga.AC2 fpga.AC3 fpga.AC4 fpga.AC6 fpga.AD1 fpga.AE17 fpga.AE1 fpga.AE5 fpga.AE6 fpga.AF15 fpga.AF19 fpga.AF2 fpga.AF3 fpga.AF4 fpga.AF5 fpga.B24 fpga.D26 fpga.E21 fpga.E25 fpga.U1 fpga.U2 fpga.V14 fpga.V1 fpga.V2 fpga.V4 fpga.W14 fpga.W1 fpga.W4 fpga.W5 fpga.W6 fpga.Y16 fpga.Y1 fpga.Y2 fpga.Y3 fpga.H2 fpga.H1 fpga.K2 fpga.K1 fpga.M2 fpga.M1 fpga.P2 fpga.P1 fpga.A4 fpga.A3 fpga.B2 fpga.B1 fpga.D2 fpga.D1 fpga.F2 fpga.F1 fpga.J10 fpga.J11 fpga.F14 fpga.A20 fpga.B20 fpga.A22 fpga.B22 fpga.A24 fpga.A23 fpga.C23 fpga.C24 fpga.B21 fpga.C21 fpga.C22 fpga.D21 fpga.Y5 fpga.V19 }
dict set pinio inlist $inlist
dict set pinio outlist $outlist
return $pinio
}
if { ![info exists pinioproc] } {
	global pinioproc 
	set pinioproc {}
}
puts "running pinioproc"
puts $pinioproc
lappend pinioproc qf2v07_io



#[{modulename:qf2v07
#,inpin:[P1\.D3.....]
#,outpin:[P2\.F3...]}
#,{modulename:digitizer3
#,inpin:[P1\.D3.....]
#,outpin:[P2\.F3...]}
#]
