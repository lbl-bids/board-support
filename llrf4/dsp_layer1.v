`timescale 1ns / 1ns

`ifndef DSP_FLAVOR
`define DSP_FLAVOR null_dsp
`endif

// This assembly of modules includes clock-domain-crossing,
// with the minimum possible amount of hardware specific code.
// It also works as a pin reducer that drifts into
// hardware-dependent territory, so I can test-synthesize
// targeting the FT256 with 173 user I/Os
// Features that can be considered hardware-specific are:
//   DDR output cells for the high speed DACs
//   AD56x4 driver for PLL control, since that runs on the DSP clock
// The caller is expected to have almost no logic in the DSP clock domain

module dsp_layer1(
	input clk,  // timespec 9.2 ns
	input tx_clk,
	input signed [15:0] adc1,
	input signed [15:0] adc2,
	input signed [15:0] adc3,
	input signed [15:0] adc4,
	output [13:0] dac1,
	output [13:0] dac2,

	inout [3:0] user_io,

	inout trig_in,
	inout trig_out,
//	output rf_on,  // for front panel
	input heartbeat,
	input lo_led,
	inout [4:1] GEEK_LED,


	input [6:0] control_addr,
	input control_strobe,
	input [31:0] control_data,

	// AD56x4 pins (drives PLLs and more?)
	output DAC_CLK,
	output [3:1] DAC_DIN,
	output DAC_CS,

	// SNS-compatible interlock I/O, generally not even populated
	input N_GOT_OK,
	output N_SEND_OK,

	// Stream of slow data: adc min/max, laser freq and set, etc.
	// Keep data path narrow for routing reasons, not logic element count
	input slow_op,
	input slow_snap,
	output [7:0] slow_out,

	output [15:0] app_config,

	// Combined data stream in tx_clk domain
	output strobe_enable,  // no lack of data in circular buffer
	input [13:0] stream_addr,
	input stream_gate,
	output [15:0] dsp_data,
    inout L0N, inout L0P,
    inout L1N, inout L1P,
    inout L2N, inout L2P,
    inout L3N, inout L3P,
    inout L4N, inout L4P,
    inout L5N, inout L5P,
    inout L6N, inout L6P,
    inout L7N, inout L7P,
	output sb_sdac_ref_sclk,
	output sb_sdac_trig,
	output [15:0] sb_sdac_out1,
	output [15:0] sb_sdac_out2,
	output [15:0] sb_sdac_out3,
	output [15:0] sb_sdac_out4,
	input [13:0] sb_v10,
	input [13:0] sb_v11,
	input [13:0] sb_v12,
	input [13:0] sb_v13,
	input [13:0] sb_v14,
	input [13:0] sb_v15,
	input [13:0] sb_v16,
	input [13:0] sb_v17,
	input [23:0] sb_v20,
	input [23:0] sb_v21,
	input [23:0] sb_v22,
	input [23:0] sb_v23,
	output sb_switch_op,
	output  [2:0] sb_photo_sw
);

// Size of buffer in circle_buf: can be adjusted based on FPGA memory capacity,
//  and reduced to make test benches shorter and easier to read
parameter circle_aw=13;

// send control interface to the DSP clock domain
wire [6:0] lb_addr;
wire lb_write;
wire [31:0] lb_data;
data_xdomain #(.size(39)) ctl_to_dsp(
	.clk_in(tx_clk), .gate_in(control_strobe), .data_in({control_addr,control_data}),
	.clk_out(clk), .gate_out(lb_write), .data_out({lb_addr,lb_data})
);

// Host-controllable register and event
`include "regmap_support.vh"
`EVT_dac_reconfig // e 27[4:4] -
`REG_en_mdac  // u 80[10:10] 0
`REG_vhf_tuner // u 29[15:0] -

// Transition to hardware
wire [15:0] mdac_val;
wire [2:0] mdac_addr;
wire [2:0] mdac_load;
wire mdac_trig, mdac_busy;

// Input slow controls are USB domain, shift registers are in DSP domain.
// Slow output is officially DSP, but structure controls timing so there's
// no problem reading it in USB domain.
wire slow_op_d, slow_snap_d;
data_xdomain #(1) slow_send(
	.clk_in(tx_clk), .gate_in(slow_op), .data_in(slow_snap),
	.clk_out(clk), .gate_out(slow_op_d), .data_out(slow_snap_d)
);

// Instantiate DSP package
wire [15:0] mon_result, decay_result_out;
wire [7:0] slow_out1;
wire mon_strobe, mon_boundary, decay_strobe;
wire decay_trig, fault_trig;
wire [15:0] dac1_out0, dac1_out1;
wire [15:0] dac2_out0, dac2_out1;
wire buf_sync;

wire [15:0] wave0_result;
wire [15:0] wave1_result;
wire [15:0] wave2_result;
wire [15:0] wave3_result;
wire [15:0] wave4_result;
wire [15:0] wave5_result;
wire [15:0] wave6_result;
wire [15:0] wave7_result;
wire [15:0] wave8_result;
wire [15:0] wave9_result;
wire [15:0] wavea_result;
wire [15:0] waveb_result;
wire [15:0] wavec_result;
wire [15:0] waved_result;
wire [15:0] wavee_result;
wire usb_clk=tx_clk;
wire wave0_available,wave1_available,wave2_available,wave3_available,wave4_available,wave5_available,wave6_available,wave7_available,wave8_available,wave9_available,wavea_available,waveb_available,wavec_available,waved_available,wavee_available;
wire wave_available;
//wire sb_sdac_ref_sclk,sb_sdac_trig;
//wire sb_switch_op;
//wire [2:0] sb_photo_sw;
//wire [15:0] sb_sdac_out1,sb_sdac_out2,sb_sdac_out3,sb_sdac_out4;
//wire [13:0] sb_v10,sb_v11,sb_v12,sb_v13,sb_v14,sb_v15,sb_v16,sb_v17;
//wire [23:0] sb_v20,sb_v21,sb_v22,sb_v23;

`DSP_FLAVOR #(.aw(circle_aw)) dsp_inst(.clk(clk),
	.adc1(adc1), .adc2(adc2), .adc3(adc3), .adc4(adc4),
	.dac1_out0(dac1_out0), .dac1_out1(dac1_out1),
	.dac2_out0(dac2_out0), .dac2_out1(dac2_out1),
	.trig_in(trig_in), .trig_out(trig_out),
	.N_GOT_OK(N_GOT_OK), .N_SEND_OK(N_SEND_OK),
	.buf_sync(buf_sync),
	.heartbeat(heartbeat),.lo_led(lo_led),.GEEK_LED(GEEK_LED),
	.decay_trig(decay_trig), .fault_trig(fault_trig),
	.lb_data(lb_data), .lb_addr(lb_addr), .lb_write(lb_write),
	.mdac_val(mdac_val), .mdac_addr(mdac_addr), .mdac_load(mdac_load),
	.mdac_trig(mdac_trig), .mdac_busy(mdac_busy),
	.mon_result(mon_result), .mon_strobe(mon_strobe), .mon_boundary(mon_boundary),
	.decay_result_out(decay_result_out), .decay_strobe(decay_strobe),
	.slow_op(slow_op_d), .slow_snap(slow_snap_d), .slow_out(slow_out1),
	.app_config(app_config), .user_io(user_io),
    .L0N(L0N),.L0P(L0P),
    .L1N(L1N),.L1P(L1P),
    .L2N(L2N),.L2P(L2P),
    .L3N(L3N),.L3P(L3P),
    .L4N(L4N),.L4P(L4P),
    .L5N(L5N),.L5P(L5P),
    .L6N(L6N),.L6P(L6P),
    .L7N(L7N),.L7P(L7P),
	.stream_addr(stream_addr),
	.stream_gate(stream_gate),
	.usb_clk(usb_clk),
	.wave0_result(wave0_result),.wave0_available(wave0_available),
	.wave1_result(wave1_result),.wave1_available(wave1_available),
	.wave2_result(wave2_result),.wave2_available(wave2_available),
	.wave3_result(wave3_result),.wave3_available(wave3_available),
	.wave4_result(wave4_result),.wave4_available(wave4_available),
	.wave5_result(wave5_result),.wave5_available(wave5_available),
	.wave6_result(wave6_result),.wave6_available(wave6_available),
	.wave7_result(wave7_result),.wave7_available(wave7_available),
	.wave8_result(wave8_result),.wave8_available(wave8_available),
	.wave9_result(wave9_result),.wave9_available(wave9_available),
	.wavea_result(wavea_result),.wavea_available(wavea_available),
	.waveb_result(waveb_result),.waveb_available(waveb_available),
	.wavec_result(wavec_result),.wavec_available(wavec_available),
	.waved_result(waved_result),.waved_available(waved_available),
	.wavee_result(wavee_result),.wavee_available(wavee_available),
//	.wave_available(wave_available),
    .sb_switch_op(sb_switch_op),.sb_photo_sw(sb_photo_sw),
    .sb_sdac_ref_sclk(sb_sdac_ref_sclk),.sb_sdac_trig(sb_sdac_trig),.sb_sdac_out1(sb_sdac_out1),.sb_sdac_out2(sb_sdac_out2),.sb_sdac_out3(sb_sdac_out3),.sb_sdac_out4(sb_sdac_out4),
    .sb_v10(sb_v10),.sb_v11(sb_v11),.sb_v12(sb_v12),.sb_v13(sb_v13),.sb_v14(sb_v14),.sb_v15(sb_v15),.sb_v16(sb_v16),.sb_v17(sb_v17),.sb_v20(sb_v20),.sb_v21(sb_v21),.sb_v22(sb_v22),.sb_v23(sb_v23)
);

dac_cells #(.width(14)) dac1_ddr(.clk(clk), .data0(dac1_out0[15:2]), .data1(dac1_out1[15:2]), .dac(dac1));
dac_cells #(.width(14)) dac2_ddr(.clk(clk), .data0(dac2_out0[15:2]), .data1(dac2_out1[15:2]), .dac(dac2));

// Start faking the transition to USB clock domain
//wire usb_clk=tx_clk;

wire [15:0] decay_out;
wire decay_read_stb = stream_gate & (stream_addr[3:1]==3'd4);
decay_buf decay_buf(
	.iclk(clk), .d_in(decay_result_out), .stb_in(decay_strobe),
	.boundary(~decay_strobe), .trig(decay_trig),
	.oclk(usb_clk), .read_addr({stream_addr[8:4],stream_addr[0]}),
	.stb_out(decay_read_stb), .d_out(decay_out)
);

wire [12:0] circle_addr = {stream_addr[13:4],stream_addr[2:0]};
wire [15:0] circle_out, circle_count, circle_stat;
wire circle_read_stb = stream_gate & ~stream_addr[3];
circle_buf #(.aw(circle_aw)) circle_buf(
	.iclk(clk), .d_in(mon_result), .stb_in(mon_strobe), .boundary(mon_boundary),
	.stop(fault_trig), .buf_sync(buf_sync),
	.oclk(usb_clk), .enable(/*strobe_enable*/), .read_addr(circle_addr),
	.d_out(circle_out), .stb_out(circle_read_stb),
	.buf_count(circle_count), .buf_stat(circle_stat)
);

// Make our own additions to slow shift register
// equivalence circle_stat: circle_fault 1, circle_wrap 1, circle_addr 14
`define SLOW_SR_LEN 4*8
`define SLOW_SR_DATA { circle_count, circle_stat }
parameter sr_length = `SLOW_SR_LEN;
reg [sr_length-1:0] slow_read=0;
always @(posedge clk) if (slow_op_d) begin
	slow_read <= slow_snap_d ? `SLOW_SR_DATA : {slow_read[sr_length-9:0],slow_out1};
end
assign slow_out = slow_read[sr_length-1:sr_length-8];
// Penultimate multiplexer for this source
//  (caller will add column 16)
reg [15:0] column15 = 0;
reg [15:0] combine_d=0;
reg [3:0] stream_ax=0;
always @(posedge usb_clk) begin
	// column15 <= column15 + 9;
	stream_ax <= stream_addr[3:0];
	combine_d <= stream_ax[3] ? (stream_ax[2] ? column15 : decay_out) : circle_out;
end
//assign dsp_data = combine_d;
/*wire [3:0] stream_addr_4=stream_addr[3:0];
assign dsp_data = (stream_addr_4==4'h0) ? wave0_result :
	(stream_addr_4==4'h1) ? wave1_result :
	(stream_addr_4==4'h2) ? wave2_result :
	(stream_addr_4==4'h3) ? wave3_result :
	(stream_addr_4==4'h4) ? wave4_result :
	(stream_addr_4==4'h5) ? wave5_result :
	(stream_addr_4==4'h6) ? wave6_result :
	(stream_addr_4==4'h7) ? wave7_result :
	(stream_addr_4==4'h8) ? wave8_result :
	(stream_addr_4==4'h9) ? wave9_result :
	(stream_addr_4==4'ha) ? wavea_result :
	(stream_addr_4==4'hb) ? waveb_result :
	(stream_addr_4==4'hc) ? wavec_result :
	(stream_addr_4==4'hd) ? waved_result :
	(stream_addr_4==4'he) ? wavee_result : 16'b0;
assign strobe_enable = |{wave0_available,wave1_available,wave2_available,wave3_available,wave4_available,wave5_available,wave6_available};
*/


reg [15:0] dsp_data_r=0;
reg strobe_enable_r=0;
reg [3:0] stream_addr_d=0;
always @(posedge usb_clk) begin
	stream_addr_d <= stream_addr[3:0];
    case (stream_addr_d)
        4'h0: dsp_data_r <= wave0_result;
        4'h1: dsp_data_r <= wave1_result;
        4'h2: dsp_data_r <= wave2_result;
        4'h3: dsp_data_r <= wave3_result;
        4'h4: dsp_data_r <= wave4_result;
        4'h5: dsp_data_r <= wave5_result;
        4'h6: dsp_data_r <= wave6_result;
		4'h7: dsp_data_r <= wave7_result;
		4'h8: dsp_data_r <= wave8_result;
		4'h9: dsp_data_r <= wave9_result;
		4'ha: dsp_data_r <= wavea_result;
		4'hb: dsp_data_r <= waveb_result;
		4'hc: dsp_data_r <= wavec_result;
		4'hd: dsp_data_r <= waved_result;
		4'he: dsp_data_r <= wavee_result;
        default: dsp_data_r <= 16'hdefa;
    endcase
//	strobe_enable_r <= wave_available;
	strobe_enable_r <= |{wave0_available,wave1_available,wave2_available,wave3_available,wave4_available,wave5_available,wave6_available};

end
assign strobe_enable= strobe_enable_r;
assign dsp_data = dsp_data_r;  // for buffered data

// AD56x4 driver for PLL control
// J20 is connected to VoutA of U806, run by DAC_DIN1
// m prefix for this DAC can be interpreted as representing "medium speed"
reg [1:0] mdac_clk_div=0;
always @(posedge clk) mdac_clk_div=mdac_clk_div+1;
ad56x4_driver4 dac123(.clk(clk), .ref_sclk(mdac_clk_div[1]),
	.reconfig(dac_reconfig), .internal_ref(1'b1),
	.sdac_trig(mdac_trig&en_mdac), .busy(mdac_busy),
	.addr1(mdac_addr), .addr2(mdac_addr), .addr3(mdac_addr),
	.voltage1(mdac_val), .voltage2(mdac_val), .voltage3(mdac_val),
	.load1(mdac_load[0]), .load2(mdac_load[1]), .load3(mdac_load[2]),
	.sclk(DAC_CLK), .sdio(DAC_DIN), .csb(DAC_CS));

endmodule
