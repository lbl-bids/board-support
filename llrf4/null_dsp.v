`timescale 1ns / 1ns

module null_dsp(
	input clk,
	input signed [15:0] adc1,
	input signed [15:0] adc2,
	input signed [15:0] adc3,
	input signed [15:0] adc4,
	output [15:0] dac1_out0,
	output [15:0] dac1_out1,
	output [15:0] dac2_out0,
	output [15:0] dac2_out1,
	input trig_in,
	output trig_out,
	input N_GOT_OK,
	output N_SEND_OK,
	output decay_trig,
	output fault_trig,
	output rf_on,  // for front panel
	input buf_sync,

	// Local Bus -- DSP clock domain (clk)
	input [31:0] lb_data,
	input [6:0] lb_addr,
	input lb_write,  // single-cycle causes a write

	// Hardware output
	output [15:0] mdac_val,
	output [2:0] mdac_addr,
	output [2:0] mdac_load,
	output mdac_trig,
	input mdac_busy,

	// Primary monitoring output to the host
	output signed [15:0] mon_result,
	output mon_strobe,
	output mon_boundary,

	// Decay waveform destined for "slow" readout
	output [15:0] decay_result_out,
	output decay_strobe,

	// Stream of slow data: adc min/max, laser freq and set
	// Keep data path narrow for routing reasons, not logic element count
	input slow_op,
	input slow_snap,
	output [7:0] slow_out,

	output [15:0] app_config,

	inout [3:0] user_io,
    inout L1P,
    inout L1N,
    inout L2P,
    inout L2N,
    inout L3P,
    inout L3N,
    inout L4N,
    inout L5P,
    inout L5N,
    inout L6P,
    inout L6N,
    inout L0P,
    inout L0N,
    inout L4P,
    inout L7P,
    inout L7N


);

assign dac1_out0=0;
assign dac1_out1=0;
assign dac2_out0=0;
assign dac2_out1=0;
assign trig_out=0;
assign decay_trig=0;
assign fault_trig=0;
assign rf_on=0;
assign mdac_val=0;
assign mdac_addr=0;
assign mdac_load=0;
assign mdac_trig=0;
assign mon_result=0;
assign mon_strobe=0;
assign mon_boundary=0;
assign decay_result_out=0;
assign decay_strobe=0;
assign slow_out=0;
assign app_config=0;

endmodule
