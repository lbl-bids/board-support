interface ifwr();
	wire dac2_sync,dac1_sync,dac_sclk,dac_din,vcxo_en;
	wire ref_clk0_p,ref_clk0_n;
	wire si570_clk_p,si570_clk_n;
	wire clk20_vcxo;
	modport hw(	inout dac2_sync,dac1_sync,dac_sclk,dac_din,vcxo_en
	,output ref_clk0_p,ref_clk0_n
	,output si570_clk_p,si570_clk_n
	,output clk20_vcxo
	);
endinterface

interface ifmarble(
	//	iffmc.sig fmc1
	//	,iffmc.sig fmc2
	//	,ifddr3.hw ddr3
	//	,ifwr.hw wr
	//	,ifrgmii.hw rgmii
	//	,ifpmod.hw pmod1
	//	,ifpmod.hw pmod2
	//	,ifqsfp.hw qsfp1
	//	,ifqsfp.hw qsfp2
	//	,ifiic.hw iic
	);
	ifddr3 ddr3();
	ifiic iic();
	ifpmod pmod1();
	ifpmod pmod2();
	ifwr wr();
	ifrgmii rgmii();
	ifgmii gmiirj45();
//iffmc fmc1();
//iffmc fmc2();

	wire clkin0,clkin12,clkin3;
	wire LD16,LD17;
	wire FPGA_RxD;
	wire FPGA_TxD;
	wire FPGA_RTS;
	wire FPGA_INT;
	wire i2c_fpga_sw_rst,EXP_INT,FPGA_MISO,FPGA_MOSI,FPGA_SSEL,FPGA_SCK;
	wire cfg_fcs,cfg_mosi,cfg_d02,cfg_d03,cfg_din;
	wire mgt115refclk0;
	wire mgt115refclk1;
	wire mgt116refclk0;
	wire mgt116refclk1;
	wire BOOT_CCLK;
	wire refclk;
	wire clk200;
	wire clk_locked;

	modport hw(input cfg_d03,cfg_d02,FPGA_MISO,EXP_INT,i2c_fpga_sw_rst,cfg_mosi,cfg_fcs,FPGA_RxD,LD16,LD17
	,output FPGA_INT,FPGA_MOSI,FPGA_RTS,FPGA_SCK,FPGA_SSEL,FPGA_TxD,cfg_din
	,output clkin0,clkin12,clkin3,mgt115refclk0,mgt115refclk1,mgt116refclk0,mgt116refclk1
	);
	modport cfg(output cfg_d03,cfg_d02,i2c_fpga_sw_rst
	,input FPGA_INT,FPGA_MOSI,FPGA_RTS,FPGA_SCK,FPGA_SSEL,FPGA_TxD,cfg_din
	,input clkin0,clkin12,clkin3,mgt115refclk0,mgt115refclk1,mgt116refclk0,mgt116refclk1
	,output BOOT_CCLK,refclk,clk200,clk_locked
	);
	modport driver(output FPGA_MISO,EXP_INT,cfg_mosi,cfg_fcs,FPGA_RxD,LD16,LD17
	,input FPGA_INT,FPGA_MOSI,FPGA_RTS,FPGA_SCK,FPGA_SSEL,FPGA_TxD,cfg_din
	,input clkin0,clkin12,clkin3,mgt115refclk0,mgt115refclk1,mgt116refclk0,mgt116refclk1
	,input BOOT_CCLK,refclk,clk200,clk_locked
	);
	modport sim(output cfg_d03,cfg_d02,FPGA_MISO,EXP_INT,i2c_fpga_sw_rst,cfg_mosi,cfg_fcs,FPGA_RxD,LD16,LD17
	,input FPGA_INT,FPGA_MOSI,FPGA_RTS,FPGA_SCK,FPGA_SSEL,FPGA_TxD,cfg_din
	,output clkin0,clkin12,clkin3,mgt115refclk0,mgt115refclk1,mgt116refclk0,mgt116refclk1
	);
endinterface


