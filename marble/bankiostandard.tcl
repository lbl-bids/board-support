proc bankiostandardproc { } {
	dict set iostandard2v5 se LVCMOS25
dict set iostandard2v5 diff LVDS_25
dict set iostandard1v5 se LVCMOS15
dict set iostandard1v5 diff DIFF_SSTL15
dict set iostandardnone se none
dict set iostandardnone diff none

dict set bankiostandard 12 $iostandard2v5
dict set bankiostandard 13 $iostandard2v5
dict set bankiostandard 14 $iostandard2v5
dict set bankiostandard 15 $iostandard2v5
dict set bankiostandard 16 $iostandard2v5
dict set bankiostandard 32 $iostandard1v5
dict set bankiostandard 33 $iostandard1v5
dict set bankiostandard 34 $iostandard1v5
dict set bankiostandard 115 $iostandardnone
dict set bankiostandard 116 $iostandardnone
# puts [dict get [dict get $bankiostandard 12] se]
return $bankiostandard
}
