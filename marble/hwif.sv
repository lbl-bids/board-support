interface hwif(
);
wire [2:0] ledrgb [7:0];
//	wire [4:0] buttons;
wire clk100;
wire clk125;
wire usersi570c0;
wire usersi570c1;
wire clk104_pl_sysref;
wire clk104_pl_clk;
wire clk104_sync_in;
wire [7:0] pmod0;
wire [7:0] pmod1;
wire [15:0] dacio;
wire [15:0] adcio;
wire gpio_sw_s;
wire gpio_sw_n;
wire gpio_sw_e;
wire gpio_sw_c;
wire gpio_sw_w;
wire gpio_dip_sw0;
wire gpio_dip_sw1;
wire gpio_dip_sw2;
wire gpio_dip_sw3;
wire gpio_dip_sw4;
wire gpio_dip_sw5;
wire gpio_dip_sw6;
wire gpio_dip_sw7;

modport hw(input ledrgb
,output clk100,clk125,usersi570c0,usersi570c1,clk104_pl_sysref,clk104_pl_clk,gpio_sw_s,gpio_sw_n,gpio_sw_e,gpio_sw_c,gpio_sw_w,gpio_dip_sw0,gpio_dip_sw1,gpio_dip_sw2,gpio_dip_sw3,gpio_dip_sw4,gpio_dip_sw5,gpio_dip_sw6,gpio_dip_sw7
,inout pmod0,pmod1,dacio,adcio,clk104_sync_in
);
modport cfg(output ledrgb
,input clk100,clk125,usersi570c0,usersi570c1,clk104_pl_sysref,clk104_pl_clk,gpio_sw_s,gpio_sw_n,gpio_sw_e,gpio_sw_c,gpio_sw_w,gpio_dip_sw0,gpio_dip_sw1,gpio_dip_sw2,gpio_dip_sw3,gpio_dip_sw4,gpio_dip_sw5,gpio_dip_sw6,gpio_dip_sw7
,inout pmod0,pmod1,dacio,adcio,clk104_sync_in
);
modport sim(output ledrgb
,output clk100,clk125,usersi570c0,usersi570c1,clk104_pl_sysref,clk104_pl_clk,gpio_sw_s,gpio_sw_n,gpio_sw_e,gpio_sw_c,gpio_sw_w,gpio_dip_sw0,gpio_dip_sw1,gpio_dip_sw2,gpio_dip_sw3,gpio_dip_sw4,gpio_dip_sw5,gpio_dip_sw6,gpio_dip_sw7
,inout pmod0,pmod1,dacio,adcio,clk104_sync_in
);
endinterface

module hwifsim(hwif.sim hw
,input clk100
,input clk125
,input usersi570c0
,input usersi570c1
,input clk104_pl_sysref
,input clk104_pl_clk
);
assign hw.clk100=clk100;
assign hw.clk125=clk125;
assign hw.usersi570c0=usersi570c0;
assign hw.usersi570c1=usersi570c1;
assign hw.clk104_pl_sysref=clk104_pl_sysref;
assign hw.clk104_pl_clk=clk104_pl_clk;

endmodule
