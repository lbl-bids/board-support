module digitizer_tb (
fmclpc P1
,fmchpc P2
,input clk
,input clk4x
,input clk2x
,input [63:0] U2_dout
,input [63:0]U3_dout
,output [13:0] data_i
,output [13:0] data_q
,output U27dir
,inout [3:0] J18_pmod_a987
,inout [3:0] J18_pmod_4321
,inout [3:0] J17_pmod_a987
,inout [3:0] J17_pmod_4321
);
parameter DEBUG="true";
parameter BUF_AW=13;
parameter cmoc_circle_aw=13;

wire [0:0] U2_dco_clk_out;
wire [0:0] U3_dco_clk_out;



ad9653_tb #(.FLIP_D(8'b11111111),.FLIP_DCO(1'b1),.FLIP_FRAME(1'b1),.BANK_CNT(1)) digitizer_U2(
.CSB(P2.LA31_P),.D0NA(P1.LA09_P),.D0NB(P1.LA05_P),.D0NC(P1.LA02_P),.D0ND(P1.LA03_P),.D0PA(P1.LA09_N),.D0PB(P1.LA05_N),.D0PC(P1.LA02_N),.D0PD(P1.LA03_N),.D1NA(P1.LA08_P),.D1NB(P1.LA07_P),.D1NC(P1.LA06_P),.D1ND(P1.LA01_P_CC),.D1PA(P1.LA08_N),.D1PB(P1.LA07_N),.D1PC(P1.LA06_N),.D1PD(P1.LA01_N_CC),.DCON(P1.LA00_P_CC),.DCOP(P1.LA00_N_CC),.FCON(P1.LA04_P),.FCOP(P1.LA04_N),.PDWN(P2.LA16_P),.SCLK(P2.LA29_N),.SDIO(P2.LA28_P),.SYNC(P2.LA33_P)
,.clk(clk),.clk4x(clk4x),.dina(U2_dout[63:48]),.dinb(U2_dout[47:32]),.dinc(U2_dout[31:16]),.dind(U2_dout[15:0]));
//,.clk_div_in(U4_dco_clk_out2)
ad9653_tb #(.FLIP_D(8'b11111111),.FLIP_DCO(1'b1),.FLIP_FRAME(1'b1),.BANK_CNT(2),.BANK_SEL({2'b0,2'b0,2'b0,2'b0,2'b1,2'b1,2'b1,2'b1})) digitizer_U3(
.D0NA(P1.LA22_P),.D0NB(P1.LA18_P_CC),.D0NC(P1.LA15_P),.D0ND(P1.LA14_P),.D0PA(P1.LA22_N),.D0PB(P1.LA18_N_CC),.D0PC(P1.LA15_N),.D0PD(P1.LA14_N),.D1NA(P1.LA20_P),.D1NB(P1.LA21_P),.D1NC(P1.LA16_P),.D1ND(P1.LA11_P),.D1PA(P1.LA20_N),.D1PB(P1.LA21_N),.D1PC(P1.LA16_N),.D1PD(P1.LA11_N),.DCON(P1.LA17_P_CC),.DCOP(P1.LA17_N_CC),.FCON(P1.LA19_P),.FCOP(P1.LA19_N),.PDWN(P2.LA16_P),.SYNC(P2.LA33_P),.CSB(P2.LA31_N),.SCLK(P2.LA29_N)//,.SDIO(P2.LA28_P)
,.clk(clk),.clk4x(clk4x),.dina(U3_dout[63:48]),.dinb(U3_dout[47:32]),.dinc(U3_dout[31:16]),.dind(U3_dout[15:0]));
//,.clk_div_in({U4_dco_clk_out2,U4_dco_clk_out2})


ad9781_tb digitizer_U4(
.CSB(P2.LA19_N),.D0N(P2.LA14_N),.D0P(P2.LA14_P),.D10N(P2.LA06_N),.D10P(P2.LA06_P),.D11N(P2.LA03_N),.D11P(P2.LA03_P),.D12N(P2.LA00_N_CC),.D12P(P2.LA00_P_CC),.D13N(P2.LA02_N),.D13P(P2.LA02_P),.D1N(P2.LA13_N),.D1P(P2.LA13_P),.D2N(P2.LA12_N),.D2P(P2.LA12_P),.D3N(P2.LA09_N),.D3P(P2.LA09_P),.D4N(P2.LA08_N),.D4P(P2.LA08_P),.D5N(P2.LA15_N),.D5P(P2.LA15_P),.D6N(P2.LA11_N),.D6P(P2.LA11_P),.D7N(P2.LA07_N),.D7P(P2.LA07_P),.D8N(P2.LA04_N),.D8P(P2.LA04_P),.D9N(P2.LA05_N),.D9P(P2.LA05_P),.DCIN(P2.LA10_N),.DCIP(P2.LA10_P),.DCON(P2.LA17_N_CC),.DCOP(P2.LA17_P_CC),.RESET(P2.LA21_N),.SDO(P2.LA19_P)//,.SCLK(P2.LA29_N),.SDIO(P2.LA29_P)
,.clk(clk2x),.data_i(data_i),.data_q(data_q));


NXP_74AVC4T245 digitizer_U27(.DIR(P2.LA28_N),.dirin(U27dir));

pmod digitizer_J18(.P1(P2.LA18_P_CC),.P10(P2.LA25_N),.P2(P2.LA18_N_CC),.P3(P2.LA23_P),.P4(P2.LA23_N),.P7(P2.LA20_P),.P8(P2.LA20_N),.P9(P2.LA25_P)
,.pmod_4321(J18_pmod_4321)
,.pmod_a987(J18_pmod_a987)
);
pmod digitizer_J17(.P1(P1.LA29_P),.P10(P1.LA26_N),.P2(P1.LA25_P),.P3(P1.LA27_P),.P4(P1.LA26_P),.P7(P1.LA29_N),.P8(P1.LA25_N),.P9(P1.LA27_N)
,.pmod_4321(J17_pmod_4321)
,.pmod_a987(J17_pmod_a987)
);

endmodule
